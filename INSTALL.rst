Introduction
------------
Any Unix/Linux compatible computer is suitable to run the **UC1b Indicators Tool**. Because the processing requires a substantial amount of computational resources, it is prefereable to install it and run it on a multiprocessor cluster. Here are some configuration guide lines:

- Preferable: 20-core and 256GB of RAM
- Minimal:     4-core and 8GB of RAM (for test purpose on small zones)

Softwares Dependency
--------------------
To simplify the installation process, using a virtual environment is greatly adviced. Anaconda virtual environment is highly recommanded even if experienced users can use other type of virtual environments. This documentation assumes the usage of an anaconda virtual environment named **nivauc1b** but another name can be used. With anaconda, to create such an environment and activate it, type:

.. code-block:: shell-session

    user@server:$ conda create -n nivauc1b python=3.6
    user@server:$ conda activate nivauc1b

The **UC1b Indicators Tool** requires the following software to be installed on the platform:

- anaconda with python 3.6 (or python 3.6 with the virtual environment suitable for the user)
- gdal version 2.2.2 or above
- git version 2.17.1 or above

In general, those softwares are already installed on standard computing platforms. If it is not the case on yours, please contact your system administrator. More details for installation can be found at:

- **anaconda with python 3.6** https://docs.anaconda.com/anaconda/install/
- **gdal:** https://gdal.org/
- **git:** https://gdal.org/


For instance, gdal python library can be installed in the current environment with the command:

.. code-block:: shell-session

    (nivauc1b) user@server:$ conda install -c conda-forge gdal


Downloading
-----------
The **UC1b Indicators Tool** can be downloaded by cloning the corresponding repository available on the NIVA project gitlab: 

.. code-block:: shell-session

    (nivauc1b) user@server:$ git clone git@gitlab.com:nivaeu/uc1b_indicators_tool.git

Libraries Requirements
----------------------
To finalyze the installation, several python libraries need to be installed. To do so, go to the **UC1b Indicators Tool** main directory then install the library listed in the "requirements.txt":

.. code-block:: shell-session

    (nivauc1b) user@server:$ cd uc1b_indicators_tool
    (nivauc1b) user@server:$ pip install -r requirements.txt

At this stage, the **UC1b Indicators Tool** is ready to be used. To check if the installation went well, go in scripts/ and type the command:

.. code-block:: shell-session

    (nivauc1b) user@server:$ ./uc1b_indicators_tool.py

It should output the following error message, normal in this context:

.. code-block:: shell-session

    #####################################################
    #                   NIVA project                    #
    #              uc1b Indicators Tool.py              #
    #    carbon flux and nitrate leaching indicators    #
    #                   Version 1.2                     #
    #             Cesbio - ASP - INRA (2020-2021)       #
    #             ludovic.arnaud@asp-public.fr          #
    #####################################################
    
    usage: uc1b_indicators_tool.py [-h] {CT1,CT2,NT1} ...
    
    positional arguments:
      {CT1,CT2,NT1}  Types of indicator (CT1, CT2 or NT1)
        CT1          Calculate NIVA uc1b carbon tier 1 indicator
        CT2          Calculate NIVA uc1b carbon tier 2 indicator (not implemented
                     yet)
        NT1          Calculate NIVA uc1b nitrate tier 1 indicator
    
    optional arguments:
      -h, --help     show this help message and exit
    usage: uc1b_indicators_tool.py [-h] {CT1,CT2,NT1} ...
    uc1b_indicators_tool.py: error: ../scripts/uc1b_indicators_tool.py: error: too few arguments
