TheiaNDVI module
================

Introduction:
-------------
This program can be used to download automatically L2A Sentinel-2 data on a given geographical zone and a given period, then construct automatically NDVI time series images. Downloading is made throught the free Theia platform (www.theia-land.fr_) where users should have credential. Note that no detailed documentation is provided because:

- Developing such a tool is not part of the UC1b,

- Theia geographical zone is not worldwild,

Advanced users should not have any problem to figure out the usage thanks to the command line help. However, if any issues, please contact us at ludovic.arnaud@asp-public.fr

API
---

.. automodule:: TheiaNDVI
   :members:
   :undoc-members:
   :show-inheritance:

.. _www.theia-land.fr: https://www.theia-land.fr/en/homepage-en/
