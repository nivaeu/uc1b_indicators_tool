.. NIVA Project - UC1b Tier 1 Carbon Indicator documentation master file, created by
   sphinx-quickstart on Fri Feb 12 16:10:42 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive. 

NIVA project - UC1b Indicators at Pixel Level
=============================================

.. toctree::
   :maxdepth: 4
   :caption: Documentation:
  
   readme
   install
   usage-ct1
   usage-nt1
   usage-ct2
   support

.. toctree::
   :maxdepth: 4
   :caption: Application Modules:
   
   uc1b_indicators_tool
   TheiaNDVI
   infolog
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
