User Guide Tier 1 Carbon Indicator
==================================

Introduction
------------

NIVA UC1b Indicator Tool is a command line interface program that allows the estimation of several agroecological indicators from remote sensing data. In the mode presented in this chapter, it can estimate annual carbon flux of agricultural parcels. From remote sensing data, it estimates the duration of active vegetation along an entire agricultural year. Then, an empirical relationship relates this duration of active vegetation to the annual carbon flux. In this mode, the tool takes Sentinel-2 NDVI time series as input (stack of raster images) and a set of parcels (from LPIS) then output the annuel carbon flux as a raster image at 10m resolution.


.. note::
   If you configured a virtual environment, do not forget to activate it at this stage.

Run test from provided data set
-------------------------------

As a first step, it is advised to try running the tool in order to calculated the *carbon indicator* with to the small data set provided in the **tests/** directory. Here are some details about the files contained in the data set that are relevant for testing the Tier 1 Carbon indicator

- tests/ndvi_time_series/T31TCJ_ROI_20181003_20190921_NDVI_time_serie.tif.
    This GEO-TIFF file contains the NDVI time series on a zone of about 2.5km x 2.5km in the south of France. Because the time series are made of 105 dates, this file contains 105 layers i.e. one layer per date. The next figure represents the first layer of this file that corresponds to NDVI values on the 2018/10/03 which is the first date of the time series (legend: color between black and white are mapped to NDVI between 0 and 1)

.. image:: uc1b-T1-NDVI.png
   :width: 400
   :align: center


- tests/ndvi_time_series/T31TCJ_ROI_20181003_20190921_dates.txt.
    This ASCII text file contains the dates of the time series, listed as YYYYMMDD strings, one line per date. Here is a sample of the first few lines of this file:
 
.. code-block:: console
  :linenos:

  20181003
  20181005
  20181006
  20181013
  20181020

 
- test/lpis/France_ROI_LPIS_2019.*.
    Those 6 files are vector shapefile in ESRI format. They contain a set of 107 parcels that overlap the 2.5km x 2.5km zone of the NDVI image time series. Those parcels are extracted from the French LPIS of 2019. The next figure displays those parcels:

.. image:: uc1b-T1-LPIS.png


.. note::
    Agricultural year N, is defined between (about) October of the year (N-1) and October the year N. Therefore to calculate the carbon indicator for the year N, NDVI images between October of the year (N-1) and October the year N, and LPIS data from the year N are needed


- tests/carbon_indicator_files/carbon_tier1_indicator_legend.txt.
    This file is a legend text file that can be used to display the CO2 flux product created by the carbon indicator tool (see next part). It is compatible with QGIS free GIS software.


To calculate the Tier 1 Carbon Indicator with this data set, type the following commands:

.. code-block:: console
    
    (nivauc1b) user@server:$ cd tests
    (nivauc1b) user@server:$ ../scripts/uc1b_indicators_tool.py CT1 -im ndvi_time_series/T31TCJ_ROI_20180901_20191025_NDVI_time_serie.tif -dl ndvi_time_series/T31TCJ_20180901_20191025_dates.txt -vec lpis/France_ROI_LPIS_2019.shp -th 0.3 -sd 20181003 -fd 20190921

Output should look similarly to

.. code-block:: console

    #####################################################
    #                   NIVA project                    #
    #              uc1b Indicators Tool.py              #
    #    carbon flux and nitrate leaching indicators    #
    #                   Version 1.2                     #
    #             Cesbio - ASP - INRA (2020-2022)       #
    #             ludovic.arnaud@inrae.fr               #
    #####################################################

    [2021-03-01 12:20:15][INFO] *** Calculating on file T31TCJ_ROI_20181003_20190921_NDVI_time_serie.tif ***
    [2021-03-01 12:20:15][INFO] Opening Rasterize image T31TCJ_ROI_20181003_20190921_NDVI_time_serie.tif (it might take a while)
    [2021-03-01 12:20:16][INFO] Opening vector Shapefile France_ROI_LPIS_2019.shp (it might take a while)
    [2021-03-01 12:20:16][INFO] Number of shapefile features found: 104
    [2021-03-01 12:20:16][INFO] Time period: [20181003,20190921]
    [2021-03-01 12:20:16][INFO] Starting 1 sequential block
    [2021-03-01 12:25:10][INFO] Exporting output raster (it might take a while)
    [2021-03-01 12:25:10][INFO] Gather information at parcel level
    [2021-03-01 12:25:11][INFO] Export in uc1b_carbon_indicator_tier1_T31TCJ_ROI_20180901_20191025_NDVI_time_serie_20181003_20190921_0.3.tif file
    [2021-03-01 12:25:11][INFO] Done

and two new files must have been created in the directory. Note that those files can also be found in the **tests/outputs/** directory for comparaison purpose:

- uc1b_carbon_indicator_tier1_T31TCJ_ROI_20180901_20191025_NDVI_time_serie_20181003_20190921_0.3.tif
    This file is the Tier 1 carbon indicator calculated at pixel level. More precisly, it is a GEO-TIFF that contains the following layers:

    - **Layer 1 - Parcel ID:** A rasterized version of the vector data in order to keep track of the correspondence between pixel and parcel. It is shown on the next image where each hue between red and blue corresponds to a number between 1 and 107.

    .. image:: uc1b-T1-PID.png
       :width: 400
       :align: center

    - **Layer 2 - Length of longest data hole:** The number of consecutive days when there is no valid NDVI data input over the calculation time period (defined for each pixel). These kinds of “data holes” are almost inevitable when manipulating optical remote sensing data. This can be for example due to clouds coverage or clound shadows. This value can be seen as a quality flag of the next two layers.


    - **Layer 3 - Number of days of active vegetation (unit: days):** The number of days of active vegetation at pixel level i.e. the number of days, for each pixel, where the NDVI is above the threshold:

    .. image:: uc1b-T1-ndays.png
       :width: 400
       :align: center


    - **Layer 4 - CO2 flux (unit: tC/ha):** The annual carbon fluxes at pixel level. Values are in tonne of carbon per hectar (tC/ha). Positive and negative values correspond to emission and fixation of CO2, respectively. The next figure shows this layer using the **carbon_tier1_indicator_legend.txt** legend file:

    .. image:: uc1b-T1-co2.png
       :width: 400
       :align: center


- uc1b_carbon_indicator_tier1_T31TCJ_ROI_20180901_20191025_NDVI_time_serie_20181003_20190921_0.3.csv
    This file contains statistics gathered at parcel level from quantities available at pixels level, especially average and standard deviation of CO2 fluxs and NDVI values. This file can be used as an entry of the parcel level carbon indicator tool available at https://gitlab.com/nivaeu/uc1b_tier1_co2. Here are the first 3 lines of the files:

    .. code-block:: console
      :linenos:
  
       ### NIVA Tier 1 - Parcel Indicator from Pixels One - Created 20210310 12:35:45 (Cesbio - ASP - IGN - 2020) ###
       #pid,n_pixels,n_valid_dates,nb_days_veg_avg,co2_flux_avg,nb_days_veg_p,co2_flux_p,date_1,ndvi_avg_1,ndvi_std_2,date_2, etc.
       1,1075,27,149.14,-2.16,133.00,-1.75,20181003,176.07,16.72,20181023,324.49,57.23,20181117,103.24,19.54,20181127,109.76,26.97,20181207,117.38,28.00,20190111,224.59,39.67,20190116,276.68,41.82,20190215,260.18,75.20,20190220,279.48,85.25,20190225,292.84,94.12,20190322,545.02,76.83,20190327,605.26,71.26,20190401,681.16,67.33,20190506,833.18,70.52,20190516,798.27,62.76,20190531,841.70,74.13,20190630,324.14,41.98,20190705,281.23,23.42,20190710,272.74,27.43,20190715,253.46,28.39,20190725,199.77,14.01,20190804,188.46,20.51,20190809,209.45,25.88,20190814,136.20,13.33,20190824,172.47,23.30,20190829,216.60,40.96,20190913,278.89,51.09

    Appart from the first 2 comments rows, each row are information associated to a given parcel. Here are the definition of each colums:

  - **pid:** Parcel identification number.
  - **n_pixels:** Parcel total number of pixels.
  - **n_valid_dates:** Number of valid dates i.e. date of image that do not contains any invalid (clouds, shadows, etc) pixels.
  - **nb_days_veg_avg:** Number of days of active vegetation averaged over the parcel.
  - **co2_flux_avg:** Annual CO2 flux averaged over the parcel.
  - **nb_days_veg_p:** Number of days of active vegetation calculated from NDVI time series averaged over the parcel.
  - **co2_flux_p:** Annual CO2 flux calculated from NDVI time series averaged over the parcel.
  - **date_1:** 1st date in YYYYMMDD format.
  - **ndvi_avg_1:** Average of the NDVI at the 1st date over the parcel.
  - **ndvi_std_2:** Standard deviation of NDVI at the 1st date over the parcel
  - **date_2:** 2st date in YYYYMMDD format.
  - **ndvi_avg_1:** Average of the NDVI at the 1st date over the parcel.
  - **ndvi_std_2:** Standard deviation of NDVI at the 2st date over the parcel

  then similar definition until **ndvi_std_107** for all the dates of the time series.

.. note::
  nb_days_veg_avg and co2_flux_avg are quantities obtained from the **Number of days of active vegetation** and the **Annual CO2 flux** at pixel level by averaging them at parcel level. Parcel averaged is made *after*.

  nb_days_veg_p and co2_flux_p are quantities calculted from NDVI averaged at parcel level. This two quantities are similar to the ones calculated by the carbon indicator tool at parcel level (https://gitlab.com/nivaeu/uc1b_tier1_co2). In this case, parcel averaged is made *before*.


.. note::
    This file contains the statistic of 75 parcels only. Those parcels are the ones that overlap completely the NDVI image. The other 32 "incomplete" parcels are discarded in the output.

.. note::
    Please note that, for testing and control purposes, the two GEO-TIFF and csv output files are included in the directory **tests/output/**.


Parameters details
------------------
Running the uc1b_indicators_tool.py script with the CT1 and "help" flags, gives information of the input parameters required to calculate the carbon indicator:

.. code-block:: console

    (nivauc1b) user@server:$ ../scripts/uc1b_indicators_tool.py CT1 -h

    #####################################################
    #                   NIVA project                    #
    #              uc1b Indicators Tool.py              #
    #    carbon flux and nitrate leaching indicators    #
    #                   Version 1.2                     #
    #             Cesbio - ASP - INRA (2020-2021)       #
    #             ludovic.arnaud@asp-public.fr          #
    #####################################################  
        
    usage: uc1b_indicators_tool.py CT1 [-h] -im IMAGE [IMAGE ...] -vec VECTOR
                                       [VECTOR ...] -dl DATESLIST [DATESLIST ...]
                                       -th THRESHOLD -sd STARTDATE -fd FINISHDATE
                                       [-pid PARCELID] [-np NPROC]
    
    optional arguments:
      -h, --help            show this help message and exit
      -im IMAGE [IMAGE ...], --image IMAGE [IMAGE ...]
                            Input images files
      -vec VECTOR [VECTOR ...], --vector VECTOR [VECTOR ...]
                            Input vector files
      -dl DATESLIST [DATESLIST ...], --dateslist DATESLIST [DATESLIST ...]
                            Input dates files list in YYYYMMDD format
      -th THRESHOLD, --threshold THRESHOLD
                            NDVI threshold
      -sd STARTDATE, --startdate STARTDATE
                            Start date (YYYYMMDD)
      -fd FINISHDATE, --finishdate FINISHDATE
                            Finish date (YYYYMMDD)
      -pid PARCELID, --parcelid PARCELID
                            Parcels identification code
      -np NPROC, --nproc NPROC
                            Number of processors (default 1)

Here are some specificities concerning those input parameters.

Required parameters:
--------------------
- IMAGE:
      Name of the image file containing the NDVI time series. The NDVI data needs to be in the form of (stacked) Sen4CAP L3B NDVI products, i.e. 10m resolution NDVI value between 0 and 1 are encoded as integer between 0 and 1000 (16-bits precision). Negative and invalid pixels (clouds, shadows...) are encoded with the -10000 value. Format: GEO-TIFF.
- VECTOR:
      Name of vector shapefile that contains a representation of the zone of interest where the carbon indicator needs to be calculated. It is expected to correspond to a complete or a filtered version of the LPIS data. Format: ESRI-shapefile. NDVI raster and vector shapefile must have the same system of projection.
- DATESLIST:
      List of dates corresponding to the ones on the NDVI time series. Each date is indicated as a YYYYMMDD string. If this file is not provided, it is assumed that the list of date is provided in the IMAGESLIST as metadata. Format: ASCII text file.
- THRESHOLD:
      NDVI threshold that characterize active vegetation. A value of 0.3 is considered as a standard value in the sense that NDVI value under this threshold are expected to correspond to bare soil.
- STARTDATE:
      Starting date where to start the computation (YYYYMMDD)
- FINISHDATE:
      Finishing date where to finish the computation (YYYYMMDD)
- PARCELID:
      Name of the column containting the (unique) parcels identification code that is used in the output product to keep a correspondance between pixel and parcel. The chosen column must be of INTERGER type or of STRING type with numerical values in it. If this parameter is not provided, original parcel FID is used.
- NPROC:
      Number of processors to run the carbon indicator tool. Default = 1.


.. note::
    Several separated zones can be treated by providing the first 3 parameters as list of value. For instance, to treat 2 zones in one step, the command should be in the form:

     ../scripts/uc1b_indicators_tool.py CT1 -il ndvi_1.tif ndvi_2.tif -dl dates_1.txt dates_2.txt -vec lpis_1.shp lpis_2.shp -th 0.3 -sd 20181003 -fd 20190921

     where the files (ndvi_1.tif,dates_1.txt,lpis_1.shp) correspond to a first zone, and (ndvi_2.tif,dates_2.txt,lpis_2.shp) correspond to a second zone.


