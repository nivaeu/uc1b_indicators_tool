User Guide Tier 1 Nitrate Indicator
===================================

Introduction
------------

NIVA UC1b Indicator Tool is a command line interface program that allows the estimation of several agroecological indicators from remote sensing data. In the mode presented in this chapter, it can estimate the risk of nitrate leaching of agricultural parcels. From the knowledge of bi-annual parcels rotation and from remote sensing data, it estimates this risk. In this mode, the tool takes Sentinel-2 NDVI time series as input (stack of raster images) as well as two sets of parcels (from LPIS : one set corresponding to the year of interest, one set corresponding the the previous year). The tool then output the nitrate leaching indicator as a raster image at 10m resolution. Details about the methodology can be found in the following documents (**/uc1b_indicators_tool/docs/source/FunctionalAnalysisNitrateLoss-Tier1_pixel-v0.2.4_forD2.4.docx**)


.. note::
   If you configured a virtual environment, do not forget to activate it at this stage.

Run test from provided data set
-------------------------------

The tool can also be tested with the *nitrate indicator* configuration thanks to the small data set provided in the **tests/** directory. Here are some details about the files contained in the data set that are relevant for testing the tool in its Tier 1 Nitrate indicator configuration

- tests/ndvi_time_series/T31TCJ_ROI_20180602_20190203_NDVI_time_serie.tif.
    As for the carbon indicator configuration, this GEO-TIFF file contains the NDVI time series on a zone of about 2.5km x 2.5km in the south of France. Because the time series are made of 76 dates, this file contains 76 layers i.e. one layer per date. The time period corresponding to this image time series covers roughly the period when catch crops are likelly to be present. NDVI information from catch crops are central in the Tier 1 Nitrate Indicateur methodology.

- tests/ndvi_time_series/T31TCJ_ROI_20180602_20190203_dates.txt.
    This ASCII text file contains the dates of the time series, listed as YYYYMMDD strings, one line per date. Here is a sample of the first few lines of this file:
 
.. code-block:: console
  :linenos:

  20181003
  20181005
  20181006
  20181013
  20181020
 
- test/lpis/France_ROI_LPIS_2018.* and test/lpis/France_ROI_LPIS_2019.*.
    Those two groups of 6 files are vector shapefiles in ESRI format. They contain a set of 106 parcels and 107 parcels, respectively, which overlap the 2.5km x 2.5km zone of the NDVI image time series. Those parcels are extracted from the French LPIS of 2018 and 2019. Two consecutive years are therefore needed to calculate the Nitrate Indicator which is based on the information on the 2-year crop rotation. Note that those files are not just bare polygons geometries. They also contain crop class information associated to each polygon. In those shapefiles, this information is contained in the field called “CODE_CULTU”, but other name can be also used. The next figure displays those parcels and the corresponding crop class label (“CODE_CULTU” field, for year 2018 in blue and for year 2019 in red):
.. image:: uc1b-T1-LPIS-2years.png
    :width: 400
    :align: center

Note that on this image, parcels between the two year almost completly ovrlap. It explain why more red is visible, the 2019 parcels being supperposed to the 2018 ones. 


- tests/nitrate_indicator_files/RPG2LUCAS.csv
    This file is used to translate the original LPIS crop nomenclature to the extended LUCAS nomenclature. Original LPIS crop nomenclature corresponds to the code used in the shapefile i.e. the codes contained in the "CODE_CULTU" field. Extended LUCAS nomenclature is the LUCAS nomenclature with the possibility to specify seasonality of crops such as winter or spring. For instance, the LUCAS code for wheat is B11. Winter wheat and spring wheat are encoded as B11w and B11s, respectively. In the file, the nomenclature conversion is indicated as a couple of value separated by a colon in the form **original_label:lucas_extended_label**. Here are some lines of the RPG2LUCAS.csv file that encode mapping from the French LPIS (RPG) to the LUCAS nomenclature:

.. code-block:: console

   BDH:B11w
   BTH:B11w
   ORH:B13w
   SGH:B14w
   AVH:B15w
   CHA:B15
   TTH:B18w
   TTP:B18s

- tests/nitrate_indicator_files/nitrate_tier1_indicator_legend.txt.
    This file is a legend text file that can be used to display the nitrate leaching product created by the tool (see next part). It is compatible with QGIS free GIS software.


To calculate the Tier 1 Nitrate Indicator with this data set, type the following commands:

.. code-block:: console
    
    (nivauc1b) user@server:$ cd tests
    (nivauc1b) user@server:$ ../scripts/uc1b_indicators_tool.py NT1 -im ndvi_time_series/T31TCJ_ROI_20180602_20190203_NDVI_time_serie.tif -dl ndvi_time_series/T31TCJ_20180602_20190203_dates.txt -vec lpis/France_ROI_LPIS_2018.shp lpis/France_ROI_LPIS_2019.shp  -cr CODE_CULTU -co lpis/RPG2LUCAS.csv -sd 20180602 -fd 20190203 

Output should look similarly to

.. code-block:: console

    #####################################################
    #                   NIVA project                    #
    #              uc1b Indicators Tool.py              #
    #    carbon flux and nitrate leaching indicators    #
    #                   Version 1.2                     #
    #             Cesbio - ASP - INRA (2020-2021)       #
    #             ludovic.arnaud@asp-public.fr          #
    #####################################################
    
    [2021-05-02 15:49:56][INFO] *** Calculating Nitrate Indicator on file ndvi_time_series/T31TCJ_ROI_20170903_20191025_NDVI_time_serie.tif ***
    [2021-05-02 15:49:56][INFO] Opening Rasterize image ndvi_time_series/T31TCJ_ROI_20170903_20191025_NDVI_time_serie.tif (it might take a while)
    [2021-05-02 15:49:57][INFO] Here
    [2021-05-02 15:49:57][INFO] Opening year N-1 vector Shapefile lpis/France_ROI_LPIS_2018.shp (it might take a while)
    [2021-05-02 15:49:57][INFO] Opening year N vector Shapefile lpis/France_ROI_LPIS_2019.shp (it might take a while)
    [2021-05-02 15:49:57][WARNING] The class MPC is not listed in the file conversion .csv file.
                                   Therefore this class will be ignored
    
    [2021-05-02 15:49:57][WARNING] The class B18w is not part of the nitrate parameter file
                                   Therefore this class will be ignored
    
    [2021-05-02 15:49:57][INFO] Starting 12 parallel blocks
    [2021-05-02 15:50:23][INFO] Combining parallel block
    [2021-05-02 15:50:23][INFO] Exporting output raster (it might take a while)
    [2021-05-02 15:50:23][INFO] Done

and a new files must have been created in the directory. Note that the file can also be found in the **outputs/** directory for comparaison purpose. Here are some details about its structure:
 
     - **Layer 1 - Parcel ID year N-1:** A rasterized version of the vector data of the year (N-1), in order to keep track of the correspondence between pixel and parcel.
 
     - **Layer 2 - Nmin:**  Score assessing the amount of nitrogen released after harvest of the year (N-1) crop. Expressed on a scale between 0 (low) and 1 (high). Main source are mineralization of crop residue and soil mineralization after harvest.   
 
     - **Layer 3 - Parcel ID year N:** A rasterized version of the vector data of the year N, in order to keep track of the correspondence between pixel and parcel.

     - **Layer 4 - Ncup:** Score assessing the amount of nitrogen taken up by the year N crop. Expressed on a scale between 0 (low) and 1 (high).

     - **Layer 5 - Length of longest data hole:** The number of consecutive days when there is no valid NDVI data input over the calculation time period (defined for each pixel). These kinds of “data holes” are almost inevitable when manipulating optical remote sensing data. This can be for example due to clouds coverage or clound shadows. This value can be seen as a quality flag of the next 5 layers.

     - **Layer 6 - NDVI init:** Initial minimal NDVI value in the crop rotation period.
    
     - **Layer 7 - NDVI Max:** Maximum NDVI value in the crop rotation period. This maximum is always find after NDVI init.
 
     - **Layer 8 - Ncup_cc:**  Score assessing the amount of nitrogen taken up by a potential catch crop. Expressed on a scale between 0 (low) and 1 (high).

     - **Layer 9 - Nmin_cc:** Score assessing the amount of nitrogen released after harvest of a potential catch crop. Expressed on a scale between 0 (low) and 1 (high).

     - **Layer 10 -  Nitrate indicator:** Score assessing nitrate leaching, calculater from layers 2, 4, 8 and 9. Expressed on a scale between 0 (low) and 1 (high).
 
     .. image:: uc1b-T1-nitrate.png
        :width: 400
        :align: center

.. note::
  Depending on the crop rotation associated to a specific pixel, the value of the layers 5 to 9 might not be defined. In this case, ther are represented as a NaN values.



Parameters details
------------------
Running the uc1b_indicators_tool.py with the NT1 and "help" flags, gives information of the input parameters required to calculate the nitrate indicator:

.. code-block:: console

   (nivauc1b) user@server:$ ../scripts/uc1b_indicators_tool.py NT1 -h

   #####################################################
   #                   NIVA project                    #
   #              uc1b Indicators Tool.py              #
   #    carbon flux and nitrate leaching indicators    #
   #                   Version 1.2                     #
   #             Cesbio - ASP - INRA (2020-2021)       #
   #             ludovic.arnaud@asp-public.fr          #
   #####################################################  
       
   usage: uc1b_indicators_tool.py NT1 [-h] -im IMAGE [IMAGE ...] -vec VECTOR
                                      [VECTOR ...] -dl DATESLIST [DATESLIST ...]
                                      -sd STARTDATE -fd FINISHDATE
                                      [-pid PARCELID] -cr CROP -co CONVERSION
                                      [-np NPROC]
   
   optional arguments:
     -h, --help            show this help message and exit
     -im IMAGE [IMAGE ...], --image IMAGE [IMAGE ...]
                           Input image files
     -vec VECTOR [VECTOR ...], --vector VECTOR [VECTOR ...]
                           Input vector file
     -dl DATESLIST [DATESLIST ...], --dateslist DATESLIST [DATESLIST ...]
                           Input dates files list in YYYYMMDD format
     -sd STARTDATE, --startdate STARTDATE
                           Start date (YYYYMMDD)
     -fd FINISHDATE, --finishdate FINISHDATE
                           Finish date (YYYYMMDD)
     -cr CROP, --crop CROP
                           Name of the crop field in the vector files
     -co CONVERSION, --conversion CONVERSION
                           File to convert crop label in the crop field to
                           extended LUCAS nomenclature
     -np NPROC, --nproc NPROC
                           Number of processors (default 1)

Here are some specificities concerning those input parameters.

Required parameters:
--------------------
- IMAGE:
      Name of the image file containing the NDVI time series. The NDVI data needs to be in the form of (stacked) Sen4CAP L3B NDVI products, i.e. 10m resolution NDVI value between 0 and 1 are encoded as integer between 0 and 1000 (16-bits precision). Negative and invalid pixels (clouds, shadows...) are encoded with the -10000 value. Format: GEO-TIFF.
- VECTOR:
      Name of vector shapefile that contains a representation of the zone of interest where the carbon indicator needs to be calculated. It is expected to correspond to a complete or a filtered version of the LPIS data. Format: ESRI-shapefile. NDVI raster and vector shapefile must have the same system of projection.
- DATESLIST:
      List of dates corresponding to the ones on the NDVI time series. Each date is indicated as a YYYYMMDD string. If this file is not provided, it is assumed that the list of date is provided in the IMAGESLIST as metadata. Format: ASCII text file.
- STARTDATE:
      Starting date where to start the computation (YYYYMMDD)
- FINISHDATE:
      Finishing date where to finish the computation (YYYYMMDD)
- CROP:
      Name of the field in the vector files that contains the crop class label code. 
- CONVERSION:
      Name of the file that contains the mapping to the extended LUCAS nomenclature. As already explained, the extended LUCAS nomenclature is the LUCAS nomenclature with the possibility to specify seasonality of crops such as winter or spring. For instance, the LUCAS code for wheat is B11. Winter wheat and spring wheat are encoded as B11w and B11s, respectively. In the file, the nomenclature conversion is indicated as a couple of value separated by a colon in the form original_label:lucas_extended_label
- NPROC:
      Number of processors to run the carbon indicator tool. Default = 1.
