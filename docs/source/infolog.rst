infolog module
==============

Convinient and colorfull log messaging module, especially with timestamps

.. automodule:: infolog
   :members:
   :undoc-members:
   :show-inheritance:
