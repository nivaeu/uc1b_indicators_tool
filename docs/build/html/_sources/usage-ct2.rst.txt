User Guide Tier 2 Carbon Indicator
====================================

Introduction
------------

This chapter presents the NIVA UC1b Indicator Tool command line interface program in the configuration mode that allows the estimation of annual carbon budget of agricultural parcels. In the NIVA project, the indicator that gives an estimation of the the carbon budget is called "Tier 2 Carbon Indicator" (CT2). The budget is calculated from the annual carbon flux (Carbon Tier 1) and FMIS data that allow the estimation of parcels carbon import and exportation: 

**CARBON_BUDGET(CT2) = CARBON_FLUX(CT1) - IMPORT + EXPORT**

.. note::
      If you configured a virtual environment, do not forget to activate it at this stage.


Run test from provided data set
-------------------------------

The tool can be tested in the Tier 2 Carbon Indicator mode thanks to the small data set provided in the **tests/** directory. Here are some details about the files contained in the data set that are relevant for testing the tool in its Tier 2 Carbon indicator mode.

- /test/output/uc1b_carbon_indicator_tier1_T31TCJ_ROI_20180901_20191025_NDVI_time_serie_20181003_20190921_0.3.tif
    This file is the Tier 1 Carbon Indicator output calculated at pixel level calculated with the tool in its CT1 mode. It is a GEO-TIFF that contains four layers (see "User Guide Tier 1 Carbon Indicator" for more details). The fourth layer contains an estimation of the annual carbon flux represented here.

.. image:: uc1b-T1-co2.png
    :width: 400
    :align: center

.. note::
     In the CT1 configuration of the tool, this file is an output file. In the CT2 configuration, this file is an input file.

- /test/FMIS/FMIS_data_fake_interventions.csv
    This file contains specific parcel intervention that relevant to estimate carbon import and export at the parcel scale. It is a csv file that use the comma as a separator. Each row correspond to a specific intervention on a given parcel at a given date. The file needs to contains the following 11 colums:
  - **PID:** Parcel identifier number. In this file, this identifier **does not** need to be unique because it might correspond to several interventions on the same parcels.
  - **DATE:** Date of the intervention. Format: YYYYMMDD.
  - **N_CROP:** Name of the crop present on the parcel at the time of the interventions.
  - **C_CROP:** Code of the crop present on the parcel at the time of the interventions. This code is picked form the LUCAS nomenclature.
  - **N_OAME:** Name of the organic amendment imported on the parcel at the time of the intervention
  - **C_OAME:** Code of the organic amendment imported on the parcel at the time of the intervention. This code must be picked from the organic amendment list consider in the CT2 methodilogy. See list in Table 1 below.
  - **M_OAME:** Mass of the organic amendment imported on the parcel at the time of the intervention. **Important:** This mass must be expressed in metric tonne per hectar (t/ha).
  - **M_GRAIN:** Mass of exported grain in t/ha.
  - **M_STRAW:** Mass of exported tuber or root in t/ha.
  - **M_TUBER:** Mass of exported straw in t/ha
  - **M_DRY:** Mass of exported dry above ground biomass in t/ha
  - **M_GREEN:** Mass of exported green above ground biomass in t/ha

.. image:: uc1b-T2-co2-amendments.png
     :width: 800
     :align: center
     
**Table 1:** List of organic amendments with their respective code.

.. note::
      Colons **N_CROP** and **N_OAME** are not mandatory. But it is adviced to include them to facilitate the edition and the checking of the intervention file.

.. note::
    Depending of the situation, not all columns need to be filled. For instance, for an amendment importation intervention, the columns **M_OAME** will be filled but none of the **M_GRAIN**, **M_STRAW**, **M_TUBER**, **M_DRY** or  **M_GREEN** columns. For a grain exportation, **M_GRAIN** needs to be filled, but **M_OAME**, **M_STRAW**, **M_TUBER**, **M_DRY** or **M_GREEN**  not.

.. warning::
    A consistent intervention data file must never have the following columns all filled for the same intervention:

    - **M_GRAIN** and **M_DRY/GREEN**

    - **M_STRAW** and **M_DRY/GREEN**

    - **M_GRAIN**, **M_STRAW** and **M_DRY/GREEN**

    - **M_GRAIN**, **M_TUBER**
     
    In all of those situations, the corresponding parcel will be discarded from the calculation and will be indicated by a **warning** message from the tool of the form:
    
    **[2022-03-04 16:34:22][WARNING]** Intervention on parcel 1253 is invalid. This parcel will be discarded from the computation



To be more concrete, here is a preview of the - /test/FMIS/FMIS_data_fake_interventions.csv

.. image:: uc1b-T2-co2-interventions.png
     :width: 900
     :align: center

.. warning::
    For confidentiality reasons, it is not possible to include authentic FMIS data in the tool examplary data set. Therefore, values in the **/test/FMIS/FMIS_data_fake_interventions.csv** are fake and do not correspond to anything scientifically valid.

- /test/FMIS/FMIS_data_fake_parcels.shp
    This file (and the other files with the same root name) is an ESRI-shapefile that contains the polygons geometries corresponding to the parcels included in the interventions file. The attribute table needs to contain the **PID** column that corresponds uniquely to the parcels included in the interventions. Here is an illustration of the intervention parcel polygons superposed with the annual carbon flux output of the CT1.

.. image:: uc1b-CT2-parcels.png
     :width: 400
     :align: center
 
.. note::
   As soon as the file contains the **PID** column, the attribute table can contain any other column. It will not interfere with the tool functionality.

To calculate the Tier 1 Carbon Indicator with this data set, type the following commands:
 
 .. code-block:: console
     
     (nivauc1b) user@server:$ cd tests
     (nivauc1b) user@server:$ ../scripts/uc1b_indicators_tool.py CT2 -ct1 outputs/uc1b_carbon_indicator_tier1_T31TCJ_ROI_20180901_20191025_NDVI_time_serie_20181003_20190921_0.3.tif -int FMIS/FMIS_data_fake_interventions.csv -par FMIS/FMIS_data_fake_parcels.shp
 
output should look similarly to
 
 .. code-block:: console

    #####################################################
    #                   NIVA project                    #
    #              uc1b Indicators Tool.py              #
    #    carbon flux and nitrate leaching indicators    #
    #                   Version 2.0                     #
    #             Cesbio - ASP - INRA (2020-2022)       #
    #             ludovic.arnaud@inrae.fr               #
    #####################################################  
        
    [2022-06-16 00:20:48][INFO] *** Calculating Carbon Indicator Tier2 from Carbon Indicator Tier1 contained in file ***
                                *** outputs/uc1b_carbon_indicator_tier1_T31TCJ_ROI_20180901_20191025_NDVI_time_serie_20181003_20190921_0.3.tif ***
    [2022-06-16 00:20:48][INFO] Opening NIVA carbon Tier 1 image outputs/uc1b_carbon_indicator_tier1_T31TCJ_ROI_20180901_20191025_NDVI_time_serie_20181003_20190921_0.3.tif (it might take a while)
    [2022-06-16 00:20:48][INFO] Exporting Carbon Tier2 raster (it might take a while)
    [2022-06-16 00:20:48][INFO] Done


and a new files called
**uc1b_carbon_indicator_tier2_from_uc1b_carbon_indicator_tier1_T31TCJ_ROI_20180901_20191025_NDVI_time_serie_20181003_20190921_0.3.tif**
must have been created in the directory. Note that this file can also be found in the **tests/outputs/** directory for comparaison purpose. This GEO-TIFF file contains the following layers:

  - **Tiers 1 Carbon Indicator (unit: tC/ha):** The value of the Carbon Tier 1 indicator, used as input value, masked with the interventions polygons i.e. the CT1 value are only available on the interventions polygons.
  - **Carbon Imports (unit: t/ha):** The value of the carbon imports evaluated from the FMIS input data. 
  - **Minimum Carbon Exports (unit: t/ha):** The minimum value of the carbon exports evaluated from the FMIS input data.
  - **Maximum Carbon Exports (unit: t/ha):** The maximum value of the carbon exports evaluated from the FMIS input data.
  - **Minimum Tiers 2 Carbon Indicator (unit: tC/ha):** The minimum estimation of the annual carbon budget obtained according to the Carbon Tier 2 methodology. Values are in tC/ha. Positive and negative values correspond to emission and fixation of CO2, respectively.
  - **Maximum Tiers 2 Carbon Indicator (unit: tC/ha):** The maximum estimation of the annual carbon budget obtained according to the Carbon Tier 2 methodology. Values are in tC/ha. Positive and negative values correspond to emission and fixation of CO2, respectively. The next figure shows this layer using the **carbon_tier1_indicator_legend.txt** legend file:

.. image:: uc1b-CT2-budget.png
    :width: 400
    :align: center

.. warning::
    Reminder: As explained above, this carbon budget map output does not have any scientific validity because of the use of "fake" FMIS intervention data.


Parameters details
------------------
Running the uc1b_indicators_tool.py script with the CT12 and "help" flags, gives information of the input parameters required to calculate the carbon budget indicator:

.. code-block:: console

    (nivauc1b) user@server:$ ../scripts/uc1b_indicators_tool.py CT2 -h

    #####################################################
    #                   NIVA project                    #
    #              uc1b Indicators Tool.py              #
    #    carbon flux and nitrate leaching indicators    #
    #                   Version 2.0                     #
    #             Cesbio - ASP - INRA (2020-2021)       #
    #             ludovic.arnaud@inrae.fr               #
    #####################################################  
        
    usage: uc1b_indicators_tool.py CT2 [-h] -ct1 CARBONTIER1 -par PARCELS -int
                                       INTERVENTIONS
    
    optional arguments:
      -h, --help            show this help message and exit
      -ct1 CARBONTIER1, --carbontier1 CARBONTIER1
                            Input image file containing carbon tier 1 indicator
      -par PARCELS, --parcels PARCELS
                            Input file contening FMIS data parcel polygons
      -int INTERVENTIONS, --interventions INTERVENTIONS
                            Input file contening FMIS intervention data
