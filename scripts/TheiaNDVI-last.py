#!/usr/bin/env python3
"""
NIVA project - UC1B
THEIA NDVI
L. Arnaud - Cesbio - ASP -2021-01-01 
"""

import os
import glob
import csv
import sys
import argparse
import time
import numpy as np
from datetime import datetime
import requests
from clint.textui import progress
import urllib
import infolog
import json
import zipfile
import rasterio

log = infolog.infolog()
config = {}

def CalculateStack_NDVI(t,sd,fd):
    """
    Calculate and stack NDVI images
    """
    log.msg("Calculate and stack NDVI for tile %s"%(t))
    # Download S2 data on the specific zone and period

       ## unzip specific images then calculate NDVI
    allzip = glob.glob("%s/SENTINEL*.zip"%(t))
    #szip = sorted(allzip)
    allzip.sort(key = lambda x: x.split("/")[-1][11:19])
    sdate = [int(z.split("/")[-1][11:19]) for z in allzip]

    first = sdate[0]
    last  = sdate[-1]

    idxmin = 0
    idxmax = 0
    for i,d in enumerate(sdate):
        if int(sd) > d:
            idxmin = i+1
        if int(fd) >= d:
            idxmax = i

    subzip = allzip[idxmin:idxmax+1]

    log.msg("Available images between %d and %d (Total: %d images)"%(first,last,len(sdate)))
    log.msg("Period chosen between %d and %d (Total: %d images)"%(sdate[idxmin],sdate[idxmax],len(sdate[idxmin:idxmax+1])))

    descriptions = []
    for i,zf in enumerate(subzip):
    #for zf in glob.glob("%s/*.zip"%(t)):
        log.msg("Extraction from %s"%(zf))
        descriptions.append("NDVI on the %s"%(zf.split("/")[-1][11:19]))

        zd = zf.split(".")[0]
        begin = zd[:37] 
        base  = begin
        if len(glob.glob("{base}*/*_FRE_B8.tif".format(base=base))) == 0:
                os.system("""unzip -o %s '*_FRE_B8.tif' -d %s"""%(zf,t))
        if len(glob.glob("{base}*/*_FRE_B4.tif".format(base=base))) == 0: 
            os.system("""unzip -o %s '*_FRE_B4.tif' -d %s"""%(zf,t))
        if len(glob.glob("{base}*/MASKS/*_CLM_R1.tif".format(base=base))) == 0: 
            os.system("""unzip -o %s '*_CLM_R1.tif' -d %s"""%(zf,t))


        base  = (glob.glob(begin+"*/")[0])
        basename = base.split("/")[-2:-1][0]
 
        b8file   = glob.glob("{base}/*_FRE_B8.tif".format(base=base))[0]
        b4file   = glob.glob("{base}/*_FRE_B4.tif".format(base=base))[0]
        maskfile = glob.glob("{base}/MASKS/*_CLM_R1.tif".format(base=base))[0]

        log.msg("NDVI calculation for %s"%(zd))
         # Calculate NDVI and apply mask


        with rasterio.open(b8file) as src:
            ras_meta = src.profile
            b8 = src.read()

        with rasterio.open(b4file) as src:
            b4 = src.read()

        with rasterio.open(maskfile) as src:
            mask = src.read()

        ndvi = (np.greater(mask,0)*-10000 + np.less(mask,1)*1000.0*(b8-b4)/(b4+b8+1e-9)).astype(np.int16)

        # FOR DEBUG
        ###print(maskfile)
        ###print(b8[0,9000,0])
        ###print(b4[0,9000,0])
        ###print(mask[0,9000,0])
        ###print(ndvi[0,9000,0])
        ###print()
        ###print(b8[0,0,9000])
        ###print(b4[0,0,9000])
        ###print(mask[0,0,9000])
        ###print(ndvi[0,0,9000])

        try:
            output_raster[i] = ndvi
        except:
            output_raster = np.zeros((len(subzip),ndvi.shape[1],ndvi.shape[2])).astype(np.int16)
            output_raster[i] = ndvi

    ras_meta['count'] = len(subzip)
    ras_meta['dtype'] = "int16"
    out_stack = "{tile}/{tile}_{first}_{last}_NDVI_STACK.tif".format(tile = t, first=sdate[idxmin], last=sdate[idxmax])

    with rasterio.open(out_stack, 'w', **ras_meta) as dst:
        #dst.update_tags(A="test")
        dst.write(output_raster)
        # Write metadata
        for i,d in enumerate(descriptions):
            dst.set_band_description(i+1, d)

    # Output date file
    datefile = "{tile}/{tile}_{first}_{last}_dates.txt".format(tile = t, first=sdate[idxmin], last=sdate[idxmax])
    with open(datefile, 'w') as f:
        for d in descriptions:
            f.write(d[12:] + '\n')

def stack_NDVI_2(t):
    """
    OBSOLETE: Stack NDVI images using rasterio
    """
    log.msg("Stack %s NDVI images for all the time period"%(t))
    ndviList = glob.glob("%s/*/*NIVA_NDVI_MASKED_2.tif"%(t))

    first = (sorted(ndviList)[0]).split("/")[-1][11:19]
    last  = (sorted(ndviList)[-1]).split("/")[-1][11:19]

    descriptions = []
    for i,n in enumerate(sorted(ndviList)):
        descriptions.append("NDVI on the %s"%(n.split("/")[-1][11:19]))
        with rasterio.open(n) as src:
            ras_meta = src.profile
            #log.msg("Raster crs: %s"%(src.crs),"DEBUG")
            #destination = Proj(src.crs)
            #transform = src.transform
            im = src.read()
            try:
                output_raster[i] = im
            except:
                output_raster = np.zeros((len(ndviList),im.shape[1],im.shape[2])).astype(np.float32)
                output_raster[i] = im

    ras_meta['count'] = len(ndviList)
    ras_meta['dtype'] = "float32"

    out_stack = "{tile}/{tile}_{first}_{last}_NDVI_STACK_2.tif".format(tile = t, first=first, last=last)

    with rasterio.open(out_stack, 'w', **ras_meta) as dst:
        #dst.update_tags(A="test")
        dst.write(output_raster)
        for i,d in enumerate(descriptions):
            dst.set_band_description(i+1, d)
    


    #cmd = "otbcli_ConcatenateImages -il {images} -out {tile}/{tile}_{first}_{last}_NDVI_STACK.tif".format(images=images, tile = t, first=first, last=last)


def stack_NDVI(t):
    """
    OBSOLETE: Stack NDVI images using OTB
    """
    log.msg("Stack %s NDVI images for all the time period"%(t))
    ndviList = glob.glob("%s/*/*NIVA_NDVI_MASKED.tif"%(t))

    images = ""
    for n in sorted(ndviList):
        images = images + " %s"%(n)

    first = (sorted(ndviList)[0]).split("/")[-1][11:19]
    last  = (sorted(ndviList)[-1]).split("/")[-1][11:19]

    cmd = "otbcli_ConcatenateImages -il {images} -out {tile}/{tile}_{first}_{last}_NDVI_STACK.tif".format(images=images, tile = t, first=first, last=last)
    os.system(cmd)

def calculate_NDVI_2(t):
    """
    OBSOLETE: Calculate NDVI using rasterio
    """
    log.msg("Calculate NDVI for tile %s"%(t))
    # Download S2 data on the specific zone and period

    ## unzip specific images then calculate NDVI
    for zf in glob.glob("%s/*.zip"%(t))[:5]:
    #for zf in glob.glob("%s/*.zip"%(t)):
        log.msg("Extraction from %s"%(zf))
        zd = zf.split(".")[0]
        #if not(os.path.exists(zd)):
        #    os.makedirs(zd)
        # Extract from zip if path does not exist

        begin = zd[:37]
        base  = (glob.glob(begin+"*/")[0])
        basename = base.split("/")[-2:-1][0]

         # Calculate NDVI and apply mask
        b8file   = glob.glob("{base}/*_FRE_B8.tif".format(base=base))[0]
        b4file   = glob.glob("{base}/*_FRE_B4.tif".format(base=base))[0]
        maskfile = glob.glob("{base}/MASKS/*_CLM_R1.tif".format(base=base))[0]
        outfile =  "{base}/{basename}_NIVA_NDVI_MASKED_2.tif".format(base=base, basename=basename)

        # Create NDVI if file does not exist.
        if(os.path.exists(outfile)):
            log.msg("NDVI calculation for %s"%(zd))

            with rasterio.open(b8file) as src:
                ras_meta = src.profile
                b4 = src.read()

            with rasterio.open(b8file) as src:
                b8 = src.read()

            with rasterio.open(b8file) as src:
                mask = src.read()

            ndvi = (np.greater(mask,0)*-10000 + np.less(mask,1)*(b8-b4)/(b4+b8)).astype(np.float32)

            #ras_meta['count'] = 
            ras_meta['dtype'] = "float32"
    
            with rasterio.open(outfile, 'w', **ras_meta) as dst:
                #dst.update_tags(A="test")
                dst.write(ndvi)
    

        #cmd = """otbcli_BandMath -il {base}/*_FRE_B8.tif {base}/*_FRE_B4.tif {base}/MASKS/*_CLM_R1.tif -out {base}/{basename}_NIVA_NDVI_MASKED.tif int16 -exp 'im3b1>0 ? -10000:1000*(im1b1-im2b1)/(im1b1+im2b1)'""".format(base=base,basename=basename)


def calculate_NDVI(t):
    """
    OBSOLUTE: Calculate NDVI using OTB
    """
    log.msg("Calculate NDVI for tile %s"%(t))
    # Download S2 data on the specific zone and period

    ## unzip specific images then calculate NDVI
    for zf in glob.glob("%s/*.zip"%(t))[:5]:
    #for zf in glob.glob("%s/*.zip"%(t)):
        log.msg("Extraction from %s"%(zf))
        zd = zf.split(".")[0]
        #if not(os.path.exists(zd)):
        #    os.makedirs(zd)
        os.system("""unzip -o %s '*_CLM_R1.tif' -d %s"""%(zf,t))
        os.system("""unzip -o %s '*_FRE_B8.tif' -d %s"""%(zf,t))
        os.system("""unzip -o %s '*_FRE_B4.tif' -d %s"""%(zf,t))

        begin = zd[:37]
        base  = (glob.glob(begin+"*/")[0])
        basename = base.split("/")[-2:-1][0]

        log.msg("NDVI calculation for %s"%(zd))
         # Calculate NDVI and apply mask
        cmd = """otbcli_BandMath -il {base}/*_FRE_B8.tif {base}/*_FRE_B4.tif {base}/MASKS/*_CLM_R1.tif -out {base}/{basename}_NIVA_NDVI_MASKED.tif int16 -exp 'im3b1>0 ? -10000:1000*(im1b1-im2b1)/(im1b1+im2b1)'""".format(base=base,basename=basename)
        os.system(cmd)



def split_query_time(dict_query,n_slices):
        date_format = '%Y-%m-%d'
        end         = datetime.strptime(dict_query['completionDate'], date_format)
        start       = datetime.strptime(dict_query['startDate']     , date_format)
        deltatime   = (end-start)/n_slices
        #retry the same thing on first slice
        for n in range(n_slices):
            query_to_add =dict_query.copy()
            query_to_add['startDate']=(start+n*deltatime).strftime(date_format)
            query_to_add['completionDate']=(start+(1+n)*deltatime).strftime(date_format)
            if 'querylist' not in locals():
                querylist=[query_to_add]
            else:
                querylist.append([query_to_add])
        return(querylist) 

def get_credential(loginfile):
    with open(loginfile,newline="") as csvfile:
        spamreader = csv.reader(csvfile, delimiter=':')
        for row in spamreader:
            try:
                if row[0] == "login":
                    login = row[1]
                if row[0] == "password":
                    pswd = row[1]
            except:
                pass

    return login,pswd

def get_token(config):
    # Get token
    payload = {'ident': config["login"], 'pass': config["pswd"]}
    response = requests.post(config["serveur"]+"/services/authenticate/", data=payload)
    return response.text

def search_query(config,dict_query):
    query = "%s/%s/api/collections/%s/search.json?"%(config["serveur"], config["resto"], config["collection"])+urllib.parse.urlencode(dict_query)
    response = requests.get(query)
    return response.json()

def stream_download(config,dict_query):

    search = search_query(config,dict_query)
    chunk_size = 4096
    features = search["features"]


    ### DEBUG to check order
    ###for f in features:
    ###    print(f["properties"]["productIdentifier"][11:19])

    ###print("DEBUG",len(features))

    if len(features) == 500:
        #!!! if len greater than 500 more than 500 images are available for the query(500=query return limit)
        #reason for recursive splitting
        #split query - Made in Taeken (2020)
        querylist=split_query_time(dict_query,2)
        #retry the same thing on first slice
        stream_download(config,querylist[0]) # Half slice 1
        stream_download(config,querylist[1]) # Half slice 2


    else:
        for fi,feat in enumerate(features):
            feature_id  = feat["id"]
            prod        = feat["properties"]["productIdentifier"]
            file_name = "%s/%s.zip"%(dict_query["location"],prod)

            headers = {"Authorization":"Bearer " + config["token"]}
            url = '%s/%s/collections/%s/%s/download/?issuerId=theia'%( config["serveur"], config["resto"], config["collection"], feature_id)
            response = requests.get(url, headers = headers, stream=True)
            total_length = int(response.headers.get('content-length'))

            # Download if file does not exist or if size different than supposed size.    
            Qdownload = True
            if os.path.exists(file_name):
                if total_length == int(os.path.getsize(file_name)):
                    Qdownload = False

            if Qdownload:
                log.msg("Downloading file %s"%(file_name))
                with open(file_name, 'wb') as f:
                    total_length = int(response.headers.get('content-length'))
                    for chunk in progress.bar(response.iter_content(chunk_size=chunk_size), expected_size=(total_length/chunk_size) + 1):
                        if chunk:
                            f.write(chunk)
                            f.flush()
                if (response.status_code):
                    print("\033[1A\033[1C Status OK ")
                else:
                    log.msg("Network issues occured","WARNING")
                    log.msg("Downloading is going to continue in 10s","WARNING")
                    config["token"] = get_token()
                    return False

            else:
                log.msg("File %s is already dowloaded"%(file_name))

    return True


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    optional = parser._action_groups.pop() # Edited this line
    required = parser.add_argument_group('required arguments')
    required.add_argument("-t", "--tiles", nargs='+', required=True, help="Sentinel-2 tile list")
    required.add_argument("-sd", "--startingdate", help="Starting date (YYYYMMDD)")
    required.add_argument("-fd", "--finishingdate", help="Finishing date (YYYYMMDD)")
    parser._action_groups.append(optional) # added this line
    args=parser.parse_args()

    if (args.tiles==None or args.startingdate==None or args.finishingdate==None):
        # Handle help message because mutual exclusive option required it.
        print("usage:   %s [-t] tiles1 tiles2 ... [-sd] starting_dates  [-fd] finaling_date"%(__file__))
        print("example: %s [-t] T31TCJ T31TCH [-sd] 20180101 [-fd] 20190101"%(__file__))
        print("%s: error: too few arguments"%(__file__))
        quit()
    
    #login,pswd=get_credential("../config/login.cfg")
    login,pswd=get_credential(os.path.dirname(os.path.abspath(__file__))+"/../config/login.cfg")

    # Configuration #
    config["login"]          = login
    config["pswd"]           = pswd
    config["serveur"]        = 'https://theia.cnes.fr/atdistrib'
    config["resto"]          = "resto2"
    config["collection"]     = "SENTINEL2"
    config["token"]          = get_token(config)
    

    # Query parameters #
    informat  ='%Y%m%d'
    outformat ='%Y-%m-%d'
    dict_query = {}
    dict_query['startDate']       = datetime.strptime(args.startingdate, informat).strftime(outformat)
    dict_query['completionDate']  = datetime.strptime(args.finishingdate, informat).strftime(outformat)

    dict_query['maxRecords']       = '500'
    dict_query['processingLevel']  = "LEVEL2A"

    for t in args.tiles:
        log.msg("   ### Tile %s ###"%(t))
        if not(os.path.exists(t)):
            os.makedirs(t)
        dict_query["location"] = t

        ## While statement to catch error in downloading
        status = False
        while not(status):
            status = stream_download(config,dict_query)
        CalculateStack_NDVI(t, args.startingdate, args.finishingdate)


    log.msg("Done")

