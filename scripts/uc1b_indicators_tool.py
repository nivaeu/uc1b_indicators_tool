#!/usr/bin/env python3

###############################################################################
#  NIVA project - UC1b                                                        #
#  Calculate Carbon and Nitrate Indicator at pixels and parcel level          #
#  Include:                                                                   #
#           - Tier 1 carbon  indicator                                        #
#           - Tier 1 nitrate indicator                                        #
#           - Tier 2 carbon  indicator  (not plugged yet)                     #
#                                                                             #
#  L. Arnaud - Cesbio - ASP -2020-09-25                                       #
###############################################################################

# Basic modules
import os,sys
import numpy as np
import math
import argparse
import importlib
import csv
import warnings
import pandas as pd
#import geopandas as gpd
#import matplotlib.pyplot as plt

# Time, dates and multiprocessing modules
import datetime
import time
import multiprocessing as mp

# SIG modules
import fiona as fio
import rasterio
from rasterio import features
from rasterio.transform import Affine
from pyproj import Proj, transform
from shapely.geometry import mapping, shape
from collections import OrderedDict

#from osgeo import ogr, osr
#try:
#    from osgeo import gdal
#except ImportError:
#    import gdal
#    import ogr
#    import osr



# specific module
import infolog

# Path variables
root_dir = os.path.dirname(os.path.abspath(__file__))
parameter_dir = root_dir + "/../parameters/"
#nitrate_param_file = parameter_dir + "CropParametersForNitrateIndictorWithCodes_Spring_WinterCrops.xlsx"
nitrate_param_file = parameter_dir + "CropParametersForNitrateIndictorWithCodes_Spring_WinterCrops-v20220629.xlsx"
ct2_param_file = parameter_dir + "ProductsParametersForTiers2-v2.0.xlsx"

# Fix some  internal parameters
log = infolog.infolog()
epsilon = 1e-10
sys.path.insert(0,os.getcwd() )
nth = 0.0 # Caution. Big value improve the risk of big gaps.

###### Global variables ######
th1 = 0.3
th2 = 0.6
Ncup_cc_min = 0.0
Ncup_cc_max = 0.6

th1_2 = 0.0
th2_2 = 0.4
Ncup_cc_min_2 = 0.0
Ncup_cc_max_2 = 0.4

min_regrow_time = 14
max_regrow_time = 42

cc_end = 45

a = 1.0
b = 2.0

absent_crop   = []
absent_param = []
temporary_grassland_lucas = ["B50","B51","B52","B53","B55"]
season = {"B16": 2, "B19a": 2, "B19b": 2, "B21":  2, "B33":  2, "B31":  2, "B22": 2, "B23a": 2, "B35b": 2, "B35c": 2,"B35a": 2,
          "B36": 2, "B15s": 2, "B11s": 2, "B12s": 2, "B13s": 2, "B19s": 2,"B41s": 2, "B41": 2, "B32s": 2}
season2code =  {"winter":0, "spring":1, "summer":2}

cc_cycle_bounds = ["0701","0901"]

version = 2.0
vyear = "2020-2022"
email = "ludovic.arnaud@inrae.fr"

NminNcupColumn="NminNcupCodeLUCAS"
ICPeriodColumn = "Period for MinMaxNDVI"
##############################

def print_cartouche(version,year,contact):
    print("""
#####################################################
#                   NIVA project                    #
#              uc1b Indicators Tool.py              #
#    carbon flux and nitrate leaching indicators    #
#                   Version %.1f                     #
#             Cesbio - ASP - INRA (%s)       #
#             %s               #
#####################################################  
    """%(version,year,contact))

def vprint(x):
    print(x+":",eval(x))

def print_npsize(x,sx):
    print("%s (GB): "%(sx),x.size * x.itemsize/(1024*1024*1024))


def running_mean(x, N):
    """ 
    Calculate running average. Use as a lowpass filter. Special boundary condition.

    Parameters
    ----------
    x: ndarray(dtype=float, ndim=(1,:))
        Values to filter
    N: int
        Number of days to average on the left and on the right

    Returns
    -------
    out : ndarray(dtype=float, ndim=(1,:))
        Filtered values
    """

    out = []

    for i,xi in enumerate(x):
        sta = i-N
        fin = i+N
        if i-N < 0:
            sta = 0
            fin = 2*i + 1
        if i+N > len(x):
            fin = len(x)
            sta = i - (fin - i)
        subx=x[sta:fin]
        out.append(np.mean(subx))

    out = np.array(out)
    return out


def detect_cycles(interp_ndvi,cycle_threshold):
    """
    Determine number and periode of vegetation cycles i.e periods above the threshold

    Parameters
    ----------
    interp_ndvi: ndarray(dtype=float, ndim=(1,:))
        Time serie to analyse
    cycle_threshold: float
        Threshold that define vegetation cycles

    Returns
    -------
    out : ndarray(dtype=float, ndim=(1,:))
        Filtered values
    """

    above  = np.greater(interp_ndvi,cycle_threshold) # TO DO improve lower detection 
    right_above = np.roll(above,1)
    left_above = np.roll(above,-1)
    right_above[0] = False
    left_above[-1] = False
    rise = np.logical_and(above,np.logical_not(right_above))
    fall = np.logical_and(above,np.logical_not(left_above))
    rise_idx = np.argwhere(rise==True).flatten()
    fall_idx = np.argwhere(fall==True).flatten()
    
    cycles = []
    for r,f in zip(rise_idx,fall_idx):
        if r!=f:
            cycles.append([r,f])

    num_cycles = len(cycles)

    #try:
    #    cycles = change_idx.reshape((num_cycles,2))
    #except:
    #    plt.plot(interp_ndvi,"r")
    #    plt.plot(interp_ndvi*0.0 + cycle_threshold,"g")
    #    plt.show()

    return num_cycles,cycles

def calc_nitrate_old(valid_tidx,alldays,inter_ndvi,threshold):
    """
    For one pixel, calculate nitrate contribution on crop rotation period with catch crop

    Parameters
    ----------
    inter_ndvi : ndarray(dtype=float, ndim=1)
        NDVI daily time serie for one pixel
    threshold: float
        NDVI threshold to trigger vegetation period

    Returns
    -------
    max_NDVI : float
        Maximum of NDVI during crop rotation period
    length_hole: int
        Length (in days) of the length ndvi hole in the crop rotation period
    Ncup_cc: float
        Value of Ncup_cc term (between 0 and 1), proportional to max_NDVI
    """

    ###dtidx = (np.roll(valid_tidx,-1) - valid_tidx)
    ###length_hole = np.max(dtidx)
    ###idxmax = np.argmax(dtidx)
    ###date_hole = int(intdate2fmt(valid_tidx[idxmax],"%m%d"))

    # Determine number of cycle on filtered time series
    LP_ndvi = running_mean(inter_ndvi, 10)
    nb_cycles, cycles =  detect_cycles(LP_ndvi,threshold)
    
    
    # Calculate integral and max
    integral = []
    maxima   = []
    for c in cycles:
        c_ndvi = inter_ndvi[c[0]:c[1]+1]
        integ =  np.sum(c_ndvi)
        if integ > 20.0:
            integral.append(np.sum(c_ndvi))
            maxima.append(np.max(c_ndvi))

    # Correction of the number of cycle long enough
    nb_cycles = len(integral)

    # Focus on three cycles rotation
    max_NDVI = np.nan
    length_hole = np.nan
    Ncup_cc     = 0.0

    if  nb_cycles == 3 or nb_cycles == 4:
        #if (integral[1] < integral[0] and integral[1] < integral[2]) or True: # Improve this decision rule
        if True: # Improve this decision rule
            max_NDVI = maxima[-2]
            Ncup_cc = Ncup(max_NDVI,th1,th2,Ncup_cc_min,Ncup_cc_max)
                
            t0 = alldays[cycles[-2][0]] # Start date of the cycle
            t1 = alldays[cycles[-2][1]] # Finish date of the cycle

            #Get Valid_tidx [t0,t1]
            valid_in_cycle = valid_tidx[np.logical_and(t0<valid_tidx,valid_tidx<t1)] 
            dtidx = (np.roll(valid_in_cycle,-1) - valid_in_cycle)
            if max_NDVI !=np.nan and len(dtidx)>1:
                length_hole = np.max(dtidx)

    # Get NDVI bound an apply to threshold
    return max_NDVI, length_hole, Ncup_cc, nb_cycles 

def calc_nitrate(inter_ndvi,threshold):
    """
    For one pixel, calculate nitrate contribution on crop rotation period with catch crop

    Parameters
    ----------
    inter_ndvi : ndarray(dtype=float, ndim=1)
        NDVI daily time serie for one pixel
    threshold: float
        NDVI threshold to trigger vegetation period

    Returns
    -------
    max_NDVI : float
        Maximum of NDVI during the crop rotation period
    length_hole: int
        Length (in days) of the length ndvi data hole in the crop rotation period
    Ncup_cc: float
        Value of Ncup_cc term (between 0 and 1), proportional to max_NDVI
    """
    #print(inter_ndvi.shape)
    #print(inter_ndvi)
    ### valid_tidx,alldays,inter_ndvi,threshold
    init_NDVI = np.min(inter_ndvi[:cc_end])
    min_time  = np.argmin(inter_ndvi[:cc_end])
    max_NDVI  = np.max(inter_ndvi[min_time:])
    max_time  = np.argmax(inter_ndvi[min_time:])

    #Ncup_cc  = Ncup(max_NDVI,th1,th2,Ncup_cc_min,Ncup_cc_max)
    Ncup_cc = Ncup((max_NDVI-init_NDVI),th1_2,th2_2,Ncup_cc_min_2,Ncup_cc_max_2)
    #duration = (max_time - min_time)
    #regrow_coef = np.clip((duration - min_regrow_time)/(max_regrow_time-min_regrow_time),0,1)
    #Ncup_cc = Ncup_cc*regrow_coef

    return max_NDVI, init_NDVI, Ncup_cc


def calc_co2_flux(inter_ndvi,threshold):
    """
    For one pixel, calculate number of day of vegetation above the NDVI threshold and CO2 fluxes

    Parameters
    ----------
    inter_ndvi : ndarray(dtype=float, ndim=1)
        NDVI daily time serie for one pixel
    thre#shold: float
        NDVI threshold to trigger vegetation period

    Returns
    -------
    nb_days_veg : ndarray(dtype=int, ndim=1)
        Number of day during the period where the NDVI value is above the threshold
    cum_days_veg: ndarray(dtype=int, ndim=1)
        Cumulated number of days above the thresold
    co2_flux : ndarray(dtype=float, ndim=1)
        CO2 flux calculated according to UC1b methodology
    nb_cycles : ndarray(dtype=float, ndim=1)
        Number of time the NDVI hits the threshold value
    """

    # Determine number of cycle on filtered time series
    LP_ndvi = running_mean(inter_ndvi, 10)
    nb_cycles,cycles =  detect_cycles(LP_ndvi,threshold)

    # Get NDVI bound an apply to threshold
    greater = np.greater_equal(inter_ndvi,threshold)
    nb_days_veg = np.sum(greater)
    cum_days_veg = np.cumsum(greater)
    co2_flux = -0.0258*nb_days_veg + 1.685 # Empiric relationship
    return nb_days_veg, cum_days_veg, co2_flux, nb_cycles


def interpolate(tidx,ndvi):
    """
    Calculate daily interpolation on a time serie. Used for NDVI time series.

    Parameters
    ----------
    tidx : ndarray(dtype=int, ndim=1)
        List of date as integer indices. Each index correspond to the number of day between the 01/01/1970 and the considered date
    ndvi : ndarray(dtype=float, ndim=1)
        NDVI time serie per pixel that match tidx position. Especially, len(tidx)=len(ndvi)

    Returns
    -------
    alldays : ndarray(dtype=int, ndim=1)
        List of contiguous date indices
    inter_ndvi : ndarray(dtype=float, ndim=1)
        NDVI time serie per pixel interpolated at a daily rate. len(alldays)=len(inter_ndvi)
    """

    days = tidx-tidx[0]
    # interpolate (linear)
    alldays = np.arange(days[-1]+1)
    inter_ndvi = np.nan*np.zeros(alldays.shape)
    inter_ndvi[days]=ndvi
    for d in alldays:
        if np.isnan(inter_ndvi[d]):
            before = inter_ndvi[d-1]
            di = d
            while np.isnan(inter_ndvi[di]):
                di += 1
            after = inter_ndvi[di]
            A = (after-before)/(di-(d-1))
            B = before - A*(d-1)
            inter_ndvi[d] = A*d+B
    alldays = alldays + tidx[0]
    return alldays,inter_ndvi

def get_geometries_csv(shapefile_name, column=None, ftype = "int"):
    """
    Extract polygons geometries in vector shapefile after applying a -5m buffering

    Parameters
    ----------
    shapefile_name : str
        Name of vector shapefile (CSV format).
    column : str
        Name of the column in the shapefile to be extracted.
        If no column is provided, the original fid will be used.
    ftype : string
        Type of the variable contain in the colum ("int", "float" or "string")

    Returns
    -------
    feat : list of tuple
        List of tuple with the (geometry,identifier) structure. The list contains roughly as many tuple than the shapefile contains polygons. The difference can be related to the effect of buffering on the geometry.
    """
    try:
        with fio.open(shapefile_name, "r") as shapefile:
            # The vector preprocessing is similar to the SEN4CAP processing chaine
            # See http://esa-sen4cap.org/sites/default/files/Sen4CAP_DDF_v1.2_AgriPractices.pdf
            log.msg("Shapefile crs: %s"%(shapefile.crs),"DEBUG")
            #feat = [(feature['geometry'], int(feature['id'])) for feature in shapefile]
            feat  = []
            table = []
            if column == None:
                for pi,feature in enumerate(shapefile):
                    geom = shape(feature['geometry'])
                    clean = geom.buffer(-5.0) # 5m buffer similar to SEN4CAP specification
                    gi = mapping(clean)['coordinates']
                    if clean.geom_type == 'Polygon' and clean.is_valid and gi != ():
                        geom = clean
                        fi = (mapping(geom), pi+1) # pi + 1 to have number from 1 to N-1
                        feat.append(fi)
                        table.append(feature["properties"])

            elif ftype == "int":
                for feature in shapefile:
                    #print("cool1",feature)
                    geom = shape(feature['geometry'])
                    #print("cool2")
                    clean = geom.buffer(-5.0) # 5m buffer similar to SEN4CAP specification
                    gi = mapping(clean)['coordinates']
                    if clean.geom_type == 'Polygon' and clean.is_valid and gi != ():
                        geom = clean
                        fi = (mapping(geom), int(feature['properties'][column]))
                        feat.append(fi)
                        table.append(feature["properties"])

            elif ftype == "string":
                for feature in shapefile:
                    geom = shape(feature['geometry'])
                    clean = geom.buffer(-5.0) # 5m buffer similar to SEN4CAP specification
                    gi = mapping(clean)['coordinates']
                    if clean.geom_type == 'Polygon' and clean.is_valid and gi != ():
                        geom = clean
                        fi = (mapping(geom), str(feature['properties'][column]))
                        feat.append(fi)
                        table.append(feature["properties"])
    except Exception as e:
        log.msg(e,"ERROR")
        sys.exit(1)

    return feat,table



def get_geometries_id(shapefile_name, column=None, ftype = "int"):
    """
    Extract polygons geometries in vector shapefile after applying a -5m buffering

    Parameters
    ----------
    shapefile_name : str
        Name of vector shapefile (ESRI format).
    column : str
        Name of the column in the shapefile to be extracted.
        If no column is provided, the original fid will be used.
    ftype : string
        Type of the variable contain in the colum ("int", "float" or "string")

    Returns
    -------
    feat : list of tuple
        List of tuple with the (geometry,identifier) structure. The list contains roughly as many tuple than the shapefile contains polygons. The difference can be related to the effect of buffering on the geometry.
    """
    try:
        with fio.open(shapefile_name, "r") as shapefile:
            # The vector preprocessing is similar to the SEN4CAP processing chaine
            # See http://esa-sen4cap.org/sites/default/files/Sen4CAP_DDF_v1.2_AgriPractices.pdf
            log.msg("Shapefile crs: %s"%(shapefile.crs),"DEBUG")
            #feat = [(feature['geometry'], int(feature['id'])) for feature in shapefile]
            feat  = []
            table = []
            if column == None:
                for pi,feature in enumerate(shapefile):
                    geom = shape(feature['geometry'])
                    clean = geom.buffer(-5.0) # 5m buffer similar to SEN4CAP specification
                    gi = mapping(clean)['coordinates']
                    if clean.geom_type == 'Polygon' and clean.is_valid and gi != ():
                        geom = clean
                        fi = (mapping(geom), pi+1) # pi + 1 to have number from 1 to N-1
                        feat.append(fi)
                        table.append(feature["properties"])

            elif ftype == "int":
                for feature in shapefile:
                    if feature['geometry'] is not None:
                        geom = shape(feature['geometry'])
                        clean = geom.buffer(-5.0) # 5m buffer similar to SEN4CAP specification
                        gi = mapping(clean)['coordinates']
                        if clean.geom_type == 'Polygon' and clean.is_valid and gi != ():
                            geom = clean
                            fi = (mapping(geom), int(feature['properties'][column]))
                            feat.append(fi)
                            table.append(feature["properties"])

            elif ftype == "string":
                for feature in shapefile:
                    geom = shape(feature['geometry'])
                    clean = geom.buffer(-5.0) # 5m buffer similar to SEN4CAP specification
                    gi = mapping(clean)['coordinates']
                    if clean.geom_type == 'Polygon' and clean.is_valid and gi != ():
                        geom = clean
                        fi = (mapping(geom), str(feature['properties'][column]))
                        feat.append(fi)
                        table.append(feature["properties"])
    except Exception as e:
        raise
        log.msg(e,"ERROR")
        sys.exit(1)

    return feat,table


def get_param_for_geometry(feature,table,crop_column,lpis2lucas,nparam,p):
    ###idx = {"Nmin":0,"Ncup":1}
    idx = {"NminCres":0,
           "NminSoil":1,
           "Ncup":2}
    newFeat = []
    grassFeat = []
    seasonFeat = []

    for f,t in zip(feature,table):
        try:
            init_crop = t[crop_column]
            crop = lpis2lucas[init_crop]
        except KeyError:
            if init_crop not in absent_crop:
                log.msg(("The class %s is not listed in the file conversion .csv file."%(init_crop),"Therefore this class will be ignored"),"WARNING")
                absent_crop.append(init_crop)
            crop = None

        try:
            newFeat.append([f[0],nparam[crop][idx[p]]])
            grassFeat.append([f[0],crop in temporary_grassland_lucas])
            seasonFeat.append([f[0],season.get(crop,0)])
            #DEBUG
            #print("OK: ",init_crop,crop,nparam[crop][idx[p]])
        except KeyError:
            if crop not in absent_param:
                log.msg(("The class %s is not part of the nitrate parameter file"%(crop),"Therefore this class will be ignored"),"WARNING")
                absent_param.append(crop)

            newFeat.append([f[0],np.nan])

    return newFeat,grassFeat,seasonFeat


def updatename(name,tag):
    """
    Update filename with a given tag. Convinient to create filename

    Parameters
    ----------
    name : str.
        Filename to be update
    tag : str.
        String to append at the end of the filename but before the extention.

    Returns
    -------
    new_name : str.
        Updated name string.

    Examples
    --------
    .. code:: python

        print(updatename("file.txt","new"))
        >file_new.txt

    """

    new_name  = name.split(".")[:-1][0] + "_%s"%(tag)  + "." + name.split(".")[-1] 
    return new_name

def reproject_features(feat,origin,destination):
    """
    Reprojecte vector geometries from one system of coordinates to an other
    CATION: Really inefficient algorithms. Suitable for small dataset (max 100 features).
    For more, shapefile et raster need to be set to the same crs.

    Parameters
    ----------
    feat : list of tuple.
        List of feature geometry to be reprojected with the (geometry,identifier) structure
    origin : str
        System of coordinates of origin. Example: "EPSG32631"
    destination : str
        System of coordinates of destination

    Returns
    -------
    newfeat : list of tuple.
        Reprojected geometries.
    """

    # CATION: Really inefficient algo. Not used. Shapefile et raster need to be set to the same crs
    newfeat = feat.copy()
    for i,f in enumerate(feat):
        coor  = f[0]['coordinates'][0]
        ncoor = newfeat[i][0]['coordinates'][0]
        for j,c in enumerate(coor):
            x = transform(origin,destination,c[0],c[1])
            newfeat[i][0]['coordinates'][0][j] = x
    return newfeat

def intdate2fmt(idx,fmt):
    """
    Convert time index relative to the Epoch, to a specific format 
    Parameters
    ----------
    idx : int
        Time index relative the Epoche i.e. January the 1st 1970.
    fmt : str
        Output date format

    Returns
    -------
    converted_date : str
        Converted date
    """
    converted_date = (datetime.datetime(1970,1,1,0,0) + datetime.timedelta(int(idx))).strftime(fmt)
    return converted_date

def nitrate_indicator(inputdata):
    """ 
    Compute Tier One Nitrate indicater on a list on pixels.
    This function is made to handle multiprocessing in a simple way

    Parameters
    ----------
    inputdata : tuple
        Container of the following variables and parameters:

    idx : int
        Process number. Indicate the current process. Can be use during devellopement and debugging
    X : ndarray(dtype=int, ndim=1)
        Pixels' list of x's coordinate
    Y : ndarray(dtype=int, ndim=1)
        Pixels' list of y's coordinate
    im : ndarray(dtype=float, ndim=3)
        NDVI image matrix. It is 3 dimensional i.e. im.shape = (number of date, size in X, size in y)
    start_im : ndarray(dtype=float, ndim=3)
        Start date index image matrix. It is 2 dimensional i.e. im.shape = (size in X, size in y)
    end_im : ndarray(dtype=float, ndim=3)
        End data index image matrix. It is 2 dimensional i.e. im.shape = (size in X, size in y)
    threshold: float
        NDVI threshold to trigger vegetation period
    tidx : ndarray(dtype=int, ndim=1)
        List of date as integer indices. Each index correspond to the number of day between the 01/01/1970 and the considered date
    sidx : int
        Starting day of the period on which the indicator need to be calculated. Given as the number of day since 01/01/1970.
    fidx : int
        Final of the period on which the indicator need to be calculated. Given as the number of day since 01/01/1970.

    Returns
    -------
    outputdata : list of objects of the form [X,Y,max_NDVI,length_hole,Ncup_cc]
        Container of the following variables:
    X : int
        Pixel's x coordinate
    Y : int
        Pixel's y coordinate
    max_NDVI : float
        Maximum of NDVI during crop rotation period
    length_hole: int
        Length (in days) of the length ndvi hole in the crop rotation period
    Ncup_cc: float
        Value of Ncup_cc term (between 0 and 1), proportional to max_NDVI
    """

    #idx,X,Y,im,start_im,end_im,threshold,tidx,sidx,fidx = inputdata
    idx,X,Y,im,start_im,end_im,threshold,tidx = inputdata
    L = im.shape[1]
    # Display by block of 5 (5 is arbitrary, can be adjusted)
    every = math.floor(L/5) 

    log.msg("Process %d is running"%(idx),"DEBUG")
    outputdata = []
    for i,(ndvi,start,end)  in enumerate(zip(im.T,start_im.T,end_im.T)):
        if i%every == 0:
            log.msg("[Process:%d][Iteration:%d/%d]"%(idx,i,L),"DEBUG")
        # Filter invalid data
        #print("im.shape",im.shape)
        valid = np.greater(ndvi,nth)
        #print("valid.shape",valid.shape)
        valid_ndvi  = ndvi[valid]
        valid_tidx =  tidx[valid]
        nb_valid = valid_tidx.shape[0]
        #print("valid_ndvi.shape",valid_ndvi.shape)
        #print("tidx",tidx[0],tidx[-1],valid_tidx[0],valid_tidx[-1])

        if start !=0 and end !=0:
            ic_period_zone = np.logical_and(np.greater_equal(valid_tidx,start),np.less_equal(valid_tidx,end))
            dtidx = (np.roll(valid_tidx[ic_period_zone],-1) - valid_tidx[ic_period_zone])
            #print("start end",start,end)
            #print("greater",np.greater_equal(valid_tidx,start))
            #print("less",np.less_equal(valid_tidx,end))
            try:
                length_hole = np.max(dtidx)
            except:
                length_hole = np.nan
        else:
            length_hole = np.nan
        #print(ndvi.shape)
        #print(valid_ndvi.shape)
        #print(ic_period_zone.shape)
        #print(dtidx)
        #length_hole = np.max(dtidx)

        if len(valid_tidx) > 0:
            alldays, inter_ndvi = interpolate(valid_tidx,valid_ndvi)
            #period = np.logical_and(np.greater_equal(alldays,sidx),np.less_equal(alldays,fidx))
            period = np.logical_and(np.greater_equal(alldays,start),np.less_equal(alldays,end))
            if len(inter_ndvi[period]) > 0:
                #nb_days_veg, cum_days_veg, co2_flux, np_cycles = calc_co2_flux(inter_ndvi[period],threshold) # cum_days_veg is not used
                #max_ndvi, length_hole, Ncup_cc, nb_cycles = calc_nitrate(valid_tidx,alldays,inter_ndvi[period],threshold) # cum_days_veg is not used
                max_ndvi, init_ndvi, Ncup_cc  = calc_nitrate(inter_ndvi[period],threshold) # cum_days_veg is not used
                #outputdata.append([X[i],Y[i],max_ndvi,length_hole,Ncup_cc,nb_cycles])
                outputdata.append([X[i],Y[i],max_ndvi,init_ndvi,length_hole,Ncup_cc])
            else:
                # Default value for invalid pixels (part 1)
                outputdata.append([X[i],Y[i],np.nan,np.nan,np.nan,np.nan])
        else:
            # Default value for invalid pixels (part 2)
            outputdata.append([X[i],Y[i],np.nan,np.nan,np.nan,np.nan])
    log.msg("Process %d is finished"%(idx),"DEBUG")
    return outputdata



def carbon_indicator(inputdata):
    """ 
    Compute Tier One Carbon indicator on a list on pixels.
    This function is made to handle multiprocessing in a simple way

    Parameters
    ----------
    inputdata : tuple
        Container of the following variables and parameters:

    idx : int
        Process number. Indicate the current process. Can be use during devellopement and debugging
    X : ndarray(dtype=int, ndim=1)
        Pixels' list of x's coordinate
    Y : ndarray(dtype=int, ndim=1)
        Pixels' list of y's coordinate
    im : ndarray(dt104ype=float, ndim=3)
        NDVI image matrix. It is 3 dimensional i.e. im.shape = (number of date, size in X, size in y)
    threshold: float
        NDVI threshold to trigger vegetation period
    tidx : ndarray(dtype=int, ndim=1)
        List of date as integer indices. Each index correspond to the number of day between the 01/01/1970 and the considered date
    sidx : int
        Starting day of the period on which the indicator need to be calculated. Given as the number of day since 01/01/1970.
    fidx : int
        Final of the period on which the indicator need to be calculated. Given as the number of day since 01/01/1970.

    Returns
    -------
    outputdata : list of objects of the form [X,Y,nb_days_veg,co2_flux]
        Container of the following variables:
    X : int
        Pixel's x coordinate
    Y : int
        Pixel's y coordinate
    nb_days_veg : int
        Number of days of vegetation above the threshold associated to the pixel
    co2_flux : float
        Annual CO2 flux associated to the pixel
    nb_cycles : int
        Number of vegetation cycle during the period. It corresponds to the number of time the interpolated NDVI hit the NDVI threshold
    validity : int
        Validity information constructed as a combinaison of 3 numbers:
        - The first date of the longuest data hole,
        - The length of the longuest data hole (in days)
        - The number of
        For instance the number 01013034 should be read as 0101,3,034. Year is not indicated cause non-ambigious on a 1 year period.
    """

    idx,X,Y,im,threshold,tidx,sidx,fidx = inputdata
    L = im.shape[1]
    # Display by block of 5 (5 is arbitrary, can be adjusted)
    every = math.floor(L/5) 

    log.msg("Process %d is running"%(idx),"DEBUG")
    outputdata = []
    for i,ndvi in enumerate(im.T):
        if i%every == 0:
            log.msg("[Process:%d][Iteration:%d/%d]"%(idx,i,L),"DEBUG")
        # Filter invalid data
        valid = np.greater(ndvi,nth)
        valid_ndvi  = ndvi[valid]
        valid_tidx =  tidx[valid]
        nb_valid = valid_tidx.shape[0]
        dtidx = (np.roll(valid_tidx,-1) - valid_tidx)
        length_hole = np.max(dtidx)
        idxmax = np.argmax(dtidx)
        date_hole = int(intdate2fmt(valid_tidx[idxmax],"%m%d"))
        validity = int("%s%03d%03d"%(date_hole,length_hole,nb_valid))
        validity = length_hole
        if len(valid_tidx) > 0:
            alldays, inter_ndvi = interpolate(valid_tidx,valid_ndvi)
            period = np.logical_and(np.greater_equal(alldays,sidx),np.less_equal(alldays,fidx))
            if len(inter_ndvi[period]) > 0:
                nb_days_veg, cum_days_veg, co2_flux, np_cycles = calc_co2_flux(inter_ndvi[period],threshold) # cum_days_veg is not used
                outputdata.append([X[i],Y[i],nb_days_veg,co2_flux,np_cycles,validity])
            else:
                # Default value for invalid pixels (part 1)
                outputdata.append([X[i],Y[i],np.nan,np.nan,np.nan,np.nan])
        else:
            # Default value for invalid pixels (part 2)
            outputdata.append([X[i],Y[i],np.nan,np.nan,np.nan,np.nan])

    log.msg("Process %d is finished"%(idx),"DEBUG")
    return outputdata


def csv_key_value(conversion_file):
    """
    Construct dictionary from csv with key-value mapping

    Parameters
    ----------
    conversion_file : 
        Filename of csv file that contain key:value mapping

    Returns
    -------
    dict : dict.
        Dictionary that contains the key-value mapping
    """

    try:
        with open(conversion_file) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=':')
            dic = {}
            for row in csv_reader:
                if row[0][0] != "#":
                    key = row[0]   # ADD str ???
                    value = row[1]
                    dic[key] = value.strip()
    except Exception as e:
        log.msg(e,"ERROR")
        sys.exit(1)

    return dic

def Ncup(NDVI,threshold1,threshold2,Ncup_cc_min,Ncup_cc_max):
    """
    Ncup fonction: it maps linearly the set [threshold1,threshold2] to the set [0,1]
    If NDVI < threshold1, it output Ncup_cc_min
    If NDVI > threshold2, it output Ncup_cc_max

    Parameters
    ----------
    NDVI : float 
        Value of NDVI.
    threshold1 : float
        Value of minimal threshold.
    threshold2 : float
        Value of maximal threshold.

    Returns
    -------
    value : float
        Value of Ncup.
    """
 
    value = np.clip(Ncup_cc_min + Ncup_cc_max*(NDVI-threshold1)/(threshold2 - threshold1),Ncup_cc_min,Ncup_cc_max)
    return value

def get_ct2_parameters(parameters_file):
    """
    Construct dictionaries from input excel file, to convert amendments and crop typ to carbon import/export

    Parameters
    ----------
    parameters_file : str
        Name of the file that contain the ct2 indicators parameters.. This file is provided with the tool.

    Returns
    -------
    param: dict.
        Dictionaries that contains the ct2 parameters mapping.
    """
    ct2_param = pd.read_excel(parameters_file,sheet_name=None)
    

    OrganicAmendments = ct2_param["OrganicAmendments"]
    CropsParameters  = ct2_param["CropsParameters"]
    
    amendement2carbon = {}
    for k in OrganicAmendments.index:
        # The 1/1000 factors in this part of the code are there cause the C content in the Organic Amendments and
        # in biomass export are expressed in gC/kg (equivalent to kgC/t). It is consitent with all masses expressed
        # in t (t/ha)
        amendement2carbon[OrganicAmendments["CODE"][k]] = OrganicAmendments["C_CONTENT"][k]*OrganicAmendments["K1"][k]/1000.0

    crop2carbonMin = {"GRAIN":{}, "STRAW":{}, "TUBER":{}, "DRY":{}, "GREEN":{}}
    crop2carbonMax = {"GRAIN":{}, "STRAW":{}, "TUBER":{}, "DRY":{}, "GREEN":{}}
    for k in CropsParameters.index:
        for t in crop2carbonMin.keys():
            val_min = (1.0 - CropsParameters["H_%s_MAX"%(t)][k])*CropsParameters["C_%s_MIN"%(t)][k]/1000.0
            if not np.isnan(val_min):
                crop2carbonMin[t][CropsParameters["CODE"][k]] = val_min
            else:
                crop2carbonMin[t][CropsParameters["CODE"][k]] = 0.0

            val_max =  (1.0 - CropsParameters["H_%s_MIN"%(t)][k])*CropsParameters["C_%s_MAX"%(t)][k]/1000.0
            if not np.isnan(val_max):
                crop2carbonMax[t][CropsParameters["CODE"][k]] = val_max
            else:
                crop2carbonMax[t][CropsParameters["CODE"][k]] = 0.0


    ##L_clucas        = crops_nitrate_param[NminNcupColumn]["Code LUCAS"]
    ##L_clucas_ext    = crops_nitrate_param[NminNcupColumn]["Code LUCAS extended"]
    ##L_Nmin_cres     = crops_nitrate_param[NminNcupColumn]['Nmin_cres']
    ##L_Nmin_soil     = crops_nitrate_param[NminNcupColumn]['Nmin_soil']
    ###L_Nmin = crops_nitrate_param["NminNupColumn"]['Nmin']
    ##L_Ncup          = crops_nitrate_param[NminNcupColumn]['Ncup']

    ##Nparam = {None:[np.nan, np.nan, np.nan]} # Default
    ##for clucas,clucas_ext,Nmin_cres,Nmin_soil,Ncup in zip(L_clucas,L_clucas_ext,L_Nmin_cres,L_Nmin_soil,L_Ncup):
    ##   # Nparam[clucas_ext] = [(a*Nmin_cres + b*Nmin_soil)/(a+b),Ncup]
    ##    #print(clucas_ext,Nmin_cres,Nmin_soil,Ncup)
    ##    Nparam[clucas_ext] = [Nmin_cres,Nmin_soil,Ncup]


    return amendement2carbon,crop2carbonMin,crop2carbonMax



def get_nitrate_parameters(parameters_file):
    """
    Construct dictionary that map LUCAS crop to nitrate parameters, from input excel file.

    Parameters
    ----------
    parameters_file : str
        Name of the file that contain the nitrate indicators parameters.. This file is provided with the tool.

    Returns
    -------
    param: dict.
        Dictionary that contains the lucas-nitrate parameters mapping.
    """
    crops_nitrate_param = pd.read_excel(parameters_file,sheet_name=None)

    L_clucas        = crops_nitrate_param[NminNcupColumn]["Code LUCAS"]
    L_clucas_ext    = crops_nitrate_param[NminNcupColumn]["Code LUCAS extended"]
    L_Nmin_cres     = crops_nitrate_param[NminNcupColumn]['Nmin_cres']
    L_Nmin_soil     = crops_nitrate_param[NminNcupColumn]['Nmin_soil']
    #L_Nmin = crops_nitrate_param["NminNupColumn"]['Nmin']
    L_Ncup          = crops_nitrate_param[NminNcupColumn]['Ncup']

    print("L_Nmin_cres",L_Nmin_cres)
    print("L_Nmin_soil",L_Nmin_soil)

    Nparam = {None:[np.nan, np.nan, np.nan]} # Default
    for clucas,clucas_ext,Nmin_cres,Nmin_soil,Ncup in zip(L_clucas,L_clucas_ext,L_Nmin_cres,L_Nmin_soil,L_Ncup):
       # Nparam[clucas_ext] = [(a*Nmin_cres + b*Nmin_soil)/(a+b),Ncup]
        #print(clucas_ext,Nmin_cres,Nmin_soil,Ncup)
        Nparam[clucas_ext] = [Nmin_cres,Nmin_soil,Ncup]


    return Nparam

def get_crop_rotation_periods(parameters_file,startstr):
    """
    Construct dictionary that determine crop rotation periods.
 
    Parameters
    ----------
    parameters_file : str
        Name of the file that contain the nitrate indicators parameters.. This file is provided with the tool.
 
    Returns
    -------
    param: dict.
        Dictionary that contains the crop rotation periods.
    """

    crop_rotation_period = pd.read_excel(parameters_file,sheet_name=None)[ICPeriodColumn]
    s1_list     = crop_rotation_period["Season Year N-1"]
    s2_list     = crop_rotation_period["Season Year N"]
    start_list  = crop_rotation_period["Start mm-dd"]
    end_list    = crop_rotation_period["End mm-dd"]

    # Harvest year of the first crop. Migth be problematic in southern hemisphere.
    y1 = int(startstr[:4])+0

    IC_periods = {None:[np.nan, np.nan]} # Default
    for s1,s2,start,end in zip(s1_list,s2_list,start_list,end_list):
       # Nparam[clucas_ext] = [(a*Nmin_cres + b*Nmin_soil)/(a+b),Ncup]
        #print(clucas_ext,Nmin_cres,Nmin_soil,Ncup)
        s1_code = season2code[s1.lower()]
        s2_code = season2code[s2.lower()]
        start_fmt = "%2d-%s"%(y1,start)
        end_fmt = "%2d-%s"%(y1,end)
        date_start = datetime.datetime.strptime(start_fmt,'%Y-%m-%d')
        date_end   = datetime.datetime.strptime(end_fmt,'%Y-%m-%d')
        if date_start>date_end:
            # Add onn year if start month > end month
            date_end = date_end.replace(year = date_end.year + 1)
        sidx = (date_start - datetime.datetime(1970,1,1)).days
        fidx = (date_end - datetime.datetime(1970,1,1)).days
        IC_periods[10*s1_code+s2_code] = [sidx,fidx]

    return IC_periods

def date2idx(d,fmt):
    return (datetime.datetime.strptime(str(d),fmt) - datetime.datetime(1970,1,1)).days

def main_tier1_nitrate_indicator(raster,vector1,vector2,threshold1,threshold2,crop_column,conversion_file,npool,start,finish,dates_file = None):
    # Extract raster NDVI data

    log.msg("Opening Rasterize image %s (it might take a while)"%(raster))
    try:
        with rasterio.open(raster) as src:
            ras_meta = src.profile
            log.msg("Raster crs: %s"%(src.crs),"DEBUG")
            destination = Proj(src.crs)
            transform = src.transform
            im = src.read(out_dtype = rasterio.int16) # Note: Band indexing start at 1
           
            # Rescale if multiplicator format
            if np.max(im) > 1:
                im = im/1000.0

            # Create needed dates structures
            DateErrorMsg = "Dates list metadata are incorrect. Please check or provide a dates list text file with the option -dl (see help)" 
            if dates_file == None:
                dates = []
                for d in src.descriptions:
                    if d == None:
                        log.msg(DateErrorMsg,"ERROR")
                        sys.exit(1)
                    if "NDVI on the" not in d:
                        log.msg(DateErrorMsg,"ERROR")
                        sys.exit(1)

                    dates.append(int(d[12:]))
                dates = np.array(dates)
                log.msg("Dates list obtained from images metadata")
                if len(dates) != im.shape[0]:
                    log.msg(DateErrorMsg,"ERROR")
                    sys.exit(1)

            else:
                dates = np.loadtxt(dates_file).astype(np.int)

    except Exception as e:
        log.msg(e,"ERROR")
        sys.exit(1)

    tidx = dates*0
    fmt = '%Y%m%d'
    for i,d in enumerate(dates):
        tidx[i] = date2idx(d,fmt)

    # Redefine period
    if(start != None):
        sidx = (datetime.datetime.strptime(start,'%Y%m%d') - datetime.datetime(1970,1,1)).days
        startstr = start
    else:
        sidx = tidx[0]
        startstr = dates[0]

    if(finish != None):
        fidx = (datetime.datetime.strptime(finish,'%Y%m%d') - datetime.datetime(1970,1,1)).days
        finishstr = finish
    else:
        fidx  = tidx[-1]
        finishstr = dates[-1]

    # Redefine period according to sidx and fidx: Important to reduice ressource.
    #print(tidx.shape)
    #print(tidx)
    period = np.logical_and(np.greater_equal(tidx,sidx),np.less_equal(tidx,fidx))
    #print(period)

    tidx = tidx[period]
    im = im[period,:]
    #print(tidx.shape)
    #print(tidx)


    # Rasterize shapefile 1 - extract pid iformation
    log.msg("Opening year N-1 vector Shapefile %s (it might take a while)"%(vector1))
    feat1,table1 = get_geometries_id(vector1)
    shape1 = im[0].shape
    log.msg("Number of feature: %d"%(len(feat1)),"DEBUG")
    parcel1 = features.rasterize(shapes=feat1, out_shape = shape1, transform = transform, fill = np.nan)
    #parcelIDs1 = np.unique(parcel1)
    #log.msg("Number of shapefile features found: %d"%(len(parcelIDs1)))

    # Rasterize shapefile 2 - extract pid iformation
    log.msg("Opening year N vector Shapefile %s (it might take a while)"%(vector2))
    feat2,table2 = get_geometries_id(vector2)
    shape2 = im[0].shape
    log.msg("Number of feature: %d"%(len(feat2)),"DEBUG")
    parcel2 = features.rasterize(shapes=feat2, out_shape = shape2, transform = transform, fill = np.nan)
    #parcelIDs2 = np.unique(parcel2)
    #log.msg("Number of shapefile features found: %d"%(len(parcelIDs2)))

    # Get dict to conversion LUCAS nomenclature    
    lpis2lucas = csv_key_value(conversion_file)
    #print(table1)
    #print(table2)

    # Get dict for nitrate parameters mapping
    nparam = get_nitrate_parameters(nitrate_param_file)

    ic_periods = get_crop_rotation_periods(nitrate_param_file,startstr)

    # Associate nitrate parameters to geometrie
    NminCresFeat1,grass1,season1 = get_param_for_geometry(feat1,table1,crop_column,lpis2lucas,nparam,"NminCres")
    NminSoilFeat1,grass1,season1 = get_param_for_geometry(feat1,table1,crop_column,lpis2lucas,nparam,"NminSoil")
    NcupFeat2,grass2,season2 = get_param_for_geometry(feat2,table2,crop_column,lpis2lucas,nparam,"Ncup")

    #print(feat1)
    #input()
    ##print(NminFeat1)
    #input()
    #print(NcupFeat2)

    #crops_nitrate_param = pd.read_excel("CropParametersForNitrateIndictorWithCodes.xlsx",sheet_name=None)
   
    # Rasterize shapefile 1 - write Nmin_cres and Nmin_soil
    raster_NminCresFeat1 = features.rasterize(shapes=NminCresFeat1, out_shape = shape1, transform = transform, fill = np.nan)
    raster_NminSoilFeat1 = features.rasterize(shapes=NminSoilFeat1, out_shape = shape1, transform = transform, fill = np.nan)
    # Rasterize shapefile 2 - write Ncup
    raster_NcupFeat2 = features.rasterize(shapes=NcupFeat2, out_shape = shape2, transform = transform, fill = np.nan)

    # Rasterize grassland mask 1
    raster_grass1 = features.rasterize(shapes=grass1, out_shape = shape1, transform = transform, fill = np.nan).astype(np.bool)
    # Rasterize grassland mask 2
    raster_grass2 = features.rasterize(shapes=grass2, out_shape = shape2, transform = transform, fill = np.nan).astype(np.bool)

    # Rasterize season mask 1
    raster_season1 = features.rasterize(shapes=season1, out_shape = shape1, transform = transform, fill = np.nan).astype(np.byte)
    # Rasterize sfidx = (datetime.datetime.strptime(finish,'%Y%m%d') - datetime.datetime(1970,1,1)).dayseasons mask 2
    raster_season2 = features.rasterize(shapes=season2, out_shape = shape2, transform = transform, fill = np.nan).astype(np.byte)

    #print(ic_periods)
    #print(raster_season1.shape)
    #print(raster_season2.shape)

    # Rasterize start and end crop_rotation periods
    raster_common_periods = 10*raster_season1 + raster_season2
    raster_start = 0*raster_common_periods.copy().astype(np.int)
    raster_end   = 0*raster_common_periods.copy().astype(np.int)
    for k in ic_periods.keys():
        try:
            raster_start[np.equal(raster_common_periods,k)] = ic_periods[k][0]
            raster_end[np.equal(raster_common_periods,k)] = ic_periods[k][1]
        except:
            pass

    # Get pixel where temporary grassland change to an other class
    grass_to_crop_mask  = np.logical_and(raster_grass1, ~raster_grass2)
    grass_to_grass_mask = np.logical_and(raster_grass1, raster_grass2)

    # Adjust Nmin_cres value on those grassland to other crop pixels
    raster_NminCresFeat1[grass_to_crop_mask] = 2.0

    # Adjust Ncup value on those grassland remains grassland
    raster_NcupFeat2[grass_to_grass_mask] = 1.0

    # Calculate raster_Nmin
    raster_NminFeat1  = (a*raster_NminCresFeat1 + b*raster_NminSoilFeat1)/(a+b)

    # Get mask coordinate for reconstruction
    mask1 = np.greater(parcel1,-1) # Normal mask
    mask2 = np.greater(parcel2,-1) # Normal mask
    mask  = np.logical_and(mask1,mask2)
    #DEBUG
    #mask = np.greater(parcel,30000) # really small subset for debugging
    #mask = np.ones(parcel.shape).astype(np.bool) # All pixels to test performances
 
    #print_npsize(im,"im before")
    X,Y = np.where(mask)
    ndvi = im[:,mask]

    #print_npsize(im,"im[:,mask] before")
    #print("Min Max ndvi:",np.min(ndvi),np.max(ndvi))
    #ndvi = (100*ndvi).astype(np.int8)
    #print("Min Max ndvi:",np.min(ndvi),np.max(ndvi))

    raster_start = raster_start[mask]
    raster_end   = raster_end[mask]

    #print_npsize(im,"im after")
    #print_npsize(raster_grass1,"raster_grass1")
    #print_npsize(raster_season1,"raster_season1")
    #print_npsize(raster_common_periods,"raster_common_periods")
    #print_npsize(raster_start,"raster_start")
   
    tag = {}
    for i,t in enumerate(tidx):
        tag[t] = dates[i]

    if npool == 1:
        # Special case for npool = 1
        log.msg("Sequential run")
        #inputdata = (0,X,Y,ndvi,raster_start,raster_end,threshold1,tidx,sidx,fidx)
        inputdata = (0,X,Y,ndvi,raster_start,raster_end,threshold1,tidx)
        outputdata = [nitrate_indicator(inputdata)]
    else:
        log.msg("Starting %d parallel blocks"%(npool))
        pool = mp.Pool(npool)
        nblock = int(len(X)/npool) + 1
        inputdata =  [[i,X[i*nblock:(i + 1)*nblock],Y[i*nblock:(i + 1)*nblock],
            ndvi[:,i*nblock:(i + 1)*nblock],
            raster_start[i*nblock:(i + 1)*nblock],
            raster_end[i*nblock:(i + 1)*nblock],threshold1,tidx] for i in range((len(X) + nblock - 1) // nblock )] # split inputdata
        outputdata = pool.map(nitrate_indicator,inputdata)
        log.msg("Combining parallel block")

    # Prepare array to receive raster data
    max_NDVI = np.nan*parcel1
    init_NDVI = np.nan*parcel1
    length_hole = np.nan*parcel1
    Ncup_cc = np.nan*parcel1
    #Ncup_cc2 = np.nan*parcel1
    #Ncycles = np.nan*parcel1
    # TODO
    #Nmin_cc = 0.0*parcel1 + 0.3 # Define like this so far. Need to implement the spetial case with grassland
    Nmin_cc = 0.0*parcel1       # Define like this so far. Need to implement the spetial case with grassland
    #Nmin_cc2 = 0.0*parcel1       # Define like this so far. Need to implement the spetial case with grassland
    N_indicator = np.nan*parcel1

    # Combine after pooling (even if npool = 1)
    for output in outputdata:
        for out in output:
            x = out[0]
            y = out[1]
            max_NDVI[x,y]      =  out[2]
            init_NDVI[x,y]     =  out[3]
            length_hole[x,y]   = out[4]
            Ncup_cc[x,y]       = out[5]
            #Ncup_cc2[x,y]      = out[6]
            #Ncycles[x,y] = out[5]
            # consider NDVI correction if NDVI exists
            if not np.isnan(max_NDVI[x,y]):
                Nmin_cc[x,y]  = nparam["B00"][0]
                #Nmin_cc2[x,y] = nparam["B00"][0]
            else:
                Ncup_cc[x,y] = 0.0
                #Ncup_cc2[x,y] = 0.0

    # Desalocate im
    im = None

    # Filter cc correction if there is no summer crop on the first year
    # example: [sum sum noNDVI win]~[1 1 nan 0] -> [0 0 0 1]
    raster_summer1_factor = (~np.equal(raster_season1,2)).astype(np.int)
    Ncup_cc  = Ncup_cc*raster_summer1_factor
    #Ncup_cc2 = Ncup_cc2*raster_summer1_factor
    
    # Nmin_cc = 0 if no CC consider i.e. Ncup_cc = 0
    
    Nmin_cc[np.less_equal(Ncup_cc,0)] = 0.0 
    Nmin_cc = Nmin_cc*raster_summer1_factor

    #Nmin_cc2[np.less_equal(Ncup_cc2,0)] = 0.0 
    #Nmin_cc2 = Nmin_cc2*raster_summer1_factor

    
    #print("np.unique(max_NDVI)",np.unique(max_NDVI))
    summer_nan = np.where(raster_season1==2, np.nan, raster_season1)
    max_NDVI = max_NDVI + summer_nan
    length_hole = length_hole + summer_nan

    #print("np.unique(summer_nan)",np.unique(summer_nan))
    #print("np.unique(raster_summer1_factor)",np.unique(raster_summer1_factor))
    #print("np.unique(max_NDVI)",np.unique(max_NDVI))

    # Final value of the nitrate indicator
    N_indicator  = 1.0 - (raster_NminFeat1 - raster_NcupFeat2) + (Ncup_cc  - Nmin_cc)
    N_indicator = np.clip(N_indicator,0.0,1.0) # Clip oiutput between 0 and 1
    #N_indicator2 = 1.0 - (raster_NminFeat1 - raster_NcupFeat2) + (Ncup_cc2 - Nmin_cc2)


    #output_raster = np.stack((parcel1,raster_NminFeat1,parcel2,raster_NcupFeat2,Ncycles,max_NDVI,length_hole,N_indicator,Ncup_cc,Nmin_cc))
    #output_raster = np.stack((parcel1,raster_NminFeat1,parcel2,raster_NcupFeat2,max_NDVI,init_NDVI,length_hole,N_indicator,Ncup_cc,Nmin_cc,N_indicator2,Ncup_cc2,Nmin_cc2))
    output_raster = np.stack((parcel1,raster_NminFeat1,parcel2,raster_NcupFeat2,length_hole,init_NDVI,max_NDVI,Ncup_cc,Nmin_cc,N_indicator))
    #print(parcel1.shape)
    #print(raster_NminFeat1.shape)
    #print(parcel2.shape)
    #print(raster_NcupFeat2.shape)
    #print(output_raster.shape)

    # Ouput in GeoTIFF format
    #descriptions = ["Parcel ID year N-1","Nmin","Parcel ID year N","Ncup","Number of cycles","NDVI_Max","Length of longest data hole","Nitrate indicator"]
    #descriptions = ["Parcel ID year N-1","Nmin","Parcel ID year N","Ncup","Number of cycles","NDVI_Max","Length of longest data hole","Nitrate indicator","Ncup_cc","Nmin_cc"]
    descriptions = ["Parcel ID year N-1","Nmin","Parcel ID year N","Ncup","Length of longest data hole","NDVI init","NDVI Max","Ncup_cc","Nmin_cc","Nitrate indicator"]
    log.msg("Exporting output raster (it might take a while)")
    #print(ras_meta['count'])
    ras_meta['count'] = 10
    ras_meta['dtype'] = "float32"
    name = raster.split("/")[-1].split(".")[0]
    output_tiff = "uc1b_nitrate_indicator_tier1_%s_%.1f_%.1f.tif"%(name,threshold1,threshold2)
    with rasterio.open(output_tiff, 'w', **ras_meta) as dst:
        #dst.update_tags(INPUTFILE_IMAGE_TIME_SERIES=raster.split("/")[-1],
        #                INPUTFILE_VECTOR_PARCELS=vector.split("/")[-1],
        #                NDVI_THRESOLD=threshold,
        #                TIME_PERIOD="[%s,%s]"%(startstr,finishstr))
        #dst.write(output_raster)
        dst.write(output_raster.astype(np.float32))
        for i,d in enumerate(descriptions):
            dst.set_band_description(i+1, d)

def test_intervention(row):
    """
    Test if the intervention format is valid
    """

    pid = row["PID"]
    # Detect if the intervention is valid in term of importation
    Qimport = (row["C_OAME"] != 0.0) and (row["M_OAME"] != 0.0)


    # Detect if the intervention is valid in term of exportation
    ### See valid export situation truth table ###
    intbin = np.not_equal(row[["M_GRAIN","M_STRAW","M_TUBER","M_DRY","M_GREEN"]].to_numpy(),0.0).astype(np.int)
    idxval = intbin.dot(np.array([16,8,4,2,1]))
    #exportMapping = np.array([0,1,1,0,1,1,1,0,1,1,1,0,1,0,0,0,1,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0]).astype(np.bool)
    exportMapping = np.array([1,1,1,0,1,1,1,0,1,1,1,0,1,0,0,0,1,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0]).astype(np.bool)
    Qexport = exportMapping[idxval] 
    #print(Qimport,intbin,idxval,Qexport)
    # Conclude if the intervention is valid
    #Qvalid = (Qimport and not Qexport) or (not Qimport and Qexport) # equivalent to Qimport XOR Qexport
    Qvalid = (Qimport or Qexport) and (row["C_CROP"] != 0.0) # equivalent to Qimport OR Qexport

    # PRINT DEBUG
    #print("%02d"%(pid),int(Qimport),int(OK),int(KO),int(Qexport))
    #print("%02d"%(pid), intbin, idxval,Qexport)
    return pid, Qimport, Qexport, Qvalid


def main_tier2_carbon_indicator(ct1_raster_file,interventions_file,parcels_file,npool):
    """
    Main function to calculate Tier 1 carbon indicator from input data file and parameters

    Parameters
    ----------0

    ct1_raster_file : str
        Name of the Carbon Tier 1 Indicator output raster image file (TIFF format)
    parcel_file : str
        Name of parcels file (csv format).
    intervention_file : str
        Name of intervention file (csv format).
    npool : int
        Number of CPUs to use in parallel for the processing
    """

    # Extract raster NDVI data
    
    log.msg("Opening NIVA carbon Tier 1 image %s (it might take a while)"%(ct1_raster_file))
    Qraster = True
    try:
        with rasterio.open(ct1_raster_file) as src:
            ras_meta = src.profile
            log.msg("Raster crs: %s"%(src.crs),"DEBUG")
            destination = Proj(src.crs)
            transform = src.transform
            ct1 = src.read(4) # Note: CT1 flux is on band 4
    except Exception as e:
        Qraster  = False
        log.msg(e,"WARNING: Carbon Tiers 1 raster image is invalid. Calculation wil continue in vector mode to evaluate delta")
        #sys.exit(1)

    # Get FMIS geometry with Id value
    #feat,table = get_geometries_id(parcels_file,"PID")
    feat,table = get_geometries_id(parcels_file,"id_par")

    log.msg("Rasterizing Parcel ID")
    if Qraster:
        parcels_id = features.rasterize(shapes=feat, out_shape = ct1.shape, transform = transform, fill = np.nan)
    ###print("DEBUG table",table)

    log.msg("Converting Parcel Data")
    # Get FMIS information
    interventions = pd.read_csv(interventions_file,delimiter=",")
    interventions = interventions.fillna(0.0)

    # Get converssion table
    # Get correspondance tables
    amend2carbon,crop2carbMin,crop2carbMax = get_ct2_parameters(ct2_param_file)

    #print(crop2carbMin)
    #print(crop2carbMax)


    # Create output Series
    simport = pd.Series()
    sexport_min = pd.Series()
    sexport_max = pd.Series()


    # Replace nan by zero
    for t in ["GRAIN","STRAW","TUBER","DRY","GREEN"]:
        interventions["M_%s"%(t)] = interventions["M_%s"%(t)].fillna(0.0)


    valid_pid = set()
    for k in interventions.index:
        pid, Qimport, Qexport, Qvalid  = test_intervention(interventions.loc[k])
        if Qvalid:
            #log.msg("Intervention on parcel %s is valid"%(pid))
            valid_pid.add(pid) 
            if Qimport:
                simport.at[k] = interventions["M_OAME"][k]*amend2carbon[interventions["C_OAME"][k]]
            if Qexport:
                try:
                    crop = interventions["C_CROP"][k]
                    sexport_min.at[k] = 0.0
                    sexport_max.at[k] = 0.0
                    for t in ["GRAIN","STRAW","TUBER","DRY","GREEN"]:
                        sexport_min.at[k] += (interventions["M_%s"%(t)][k]*crop2carbMin[t][crop])
                        sexport_max.at[k] += (interventions["M_%s"%(t)][k]*crop2carbMax[t][crop])
                except:
                    print(k)
                    raise
        else:
            if pid in valid_pid:
                valid_pid.remove(pid)
            log.msg("Intervention on parcel %s is invalid. This parcel will be discarded from the computation"%(pid),"WARNING")

    # Update intervation dataframe with import and export and deals with nan
    interventions["IMPORT"] = simport
    interventions["IMPORT"] = interventions["IMPORT"].fillna(0.0)
    interventions["EXPORT_MIN"] = sexport_min
    interventions["EXPORT_MIN"] = interventions["EXPORT_MIN"].fillna(0.0)
    interventions["EXPORT_MAX"] = sexport_max
    interventions["EXPORT_MAX"] = interventions["EXPORT_MAX"].fillna(0.0)

    # Gather only import and export information then group by PID
    ct2_corrections = interventions[["PID","IMPORT","EXPORT_MIN","EXPORT_MAX"]]
    sum_per_pid = ct2_corrections.groupby(['PID']).sum()

    # Convertion for import to raster
    import_feat     = []
    export_min_feat = []
    export_max_feat = []
    shapefile_feat  = []
    for i,(t,f) in enumerate(zip(table,feat)):
        geo,pid = f
        selected_pid = sum_per_pid.loc[sum_per_pid.index == pid]
        try:
            pid_import = selected_pid["IMPORT"].values[0]
        except:
            pid_import = 0.0
        try:
            pid_export_min = selected_pid["EXPORT_MIN"].values[0]
        except:
            pid_export_min = 0.0
        try:
            pid_export_max = selected_pid["EXPORT_MAX"].values[0]
        except:
            pid_export_max = 0.0

        #if pid == 7581251:
        #    print(pid_import,pid_export_min,pid_export_max)
        #    pid_import = 9.97
        #    pid_export_min = 9.98
        #    pid_export_max = 9.99
        #print(pid,pid_import)
        import_feat.append((geo,pid_import))
        export_min_feat.append((geo,pid_export_min))
        export_max_feat.append((geo,pid_export_max))

        try:
            ExploitationID = 'idexploita'
            ParcelID = 'idparcelle'
            Crop = 'culture'
            prop = OrderedDict([(ExploitationID, t[ExploitationID]),
                                (ParcelID, t[ParcelID]), 
                                (Crop, t[Crop]),
                                ('import', pid_import),
                                ('min_export', pid_export_min),
                                ('max_export', pid_export_max)])

            output_schema = {'properties': OrderedDict([(ExploitationID, 'int:10'),
                                                              (ParcelID, 'int:10'),
                                                                 (Crop, 'str:254'),
                                                               ('import', 'float'),
                                                           ('min_export', 'float'),
                                                           ('max_export', 'float')]), 'geometry': 'Polygon'}

        except:
            ExploitationName = 'nom_exp'
            ParcelID = 'id_par'
            Crop = 'label_cul'

            prop = OrderedDict([(ExploitationName, t[ExploitationName]),
                                (ParcelID, (t[ParcelID])), 
                                (Crop, t[Crop]),
                                ('import', pid_import),
                                ('min_export', pid_export_min),
                                ('max_export', pid_export_max)])

            output_schema = {'properties': OrderedDict([(ExploitationName, 'str:254'),
                                                                (ParcelID, 'int:10'),
                                                                   (Crop, 'str:254'),
                                                                 ('import', 'float'),
                                                             ('min_export', 'float'),
                                                           ('max_export', 'float')]), 'geometry': 'Polygon'}


        f_dict = {'type': 'Feature', 'id': str(i), 'properties': prop, 'geometry': geo}
        if not((pid_import==0) and (pid_export_min==0) and (pid_export_max==0)):
            shapefile_feat.append(f_dict) 


    tag = interventions_file[:-4].split("/")[-1]
    output_shape = "uc1b_carbon_indicator_tier2_from_%s.shp"%(tag)
    log.msg("Exporting Carbon Tier2 Delta to shapefile %s"%(output_shape))
    with fio.open(parcels_file) as src:
        source_driver = src.driver
        source_crs = src.crs
        source_schema = src.schema

    
    # Export to shapefile

    with fio.open(output_shape,'w',driver=source_driver,crs=source_crs, schema=output_schema) as out:
        for sf in shapefile_feat:
            out.write(sf)


    if Qraster:
        raster_import      = features.rasterize(shapes=import_feat, out_shape = ct1.shape, transform = transform, fill = np.nan)
        raster_export_min  = features.rasterize(shapes=export_min_feat, out_shape = ct1.shape, transform = transform, fill = np.nan)
        raster_export_max  = features.rasterize(shapes=export_max_feat, out_shape = ct1.shape, transform = transform, fill = np.nan)
        #Note for C and CO2 conversion
        # 1 mol of C = 12g
        # 1 mol of CO2 = (12+16*2)g = 44g
        # Therefore M gCO2 => (12/44) M gC ~ 0.273 M gC
        #
        # t/ha = 1000kg/(100m x 100m) = (1000x1000)g/(100x100)m² = (1000000/10000) g/m² = 100 g/m² 
        # 

        # Mask CT1
        #mask = np.greater(parcels_id,-1)
        mask = (parcels_id/parcels_id) # PID pixel becomes 1, nans remain nans
        masked_ct1 = mask*ct1
        
        # Calculate CT2
        ct2_min = masked_ct1 - raster_import + raster_export_min
        ct2_max = masked_ct1 - raster_import + raster_export_max


        output_raster = np.stack((masked_ct1,raster_import,raster_export_min, raster_export_max,ct2_min,ct2_max))
        ###print(ct1.shape,parcels_id.shape,output_raster.shape)

        # Ouput in GeoTIFF format
        descriptions = ["Tiers 1 Carbon Indicator (unit: tC/ha)",
                        "Carbon Imports (unit: t/ha)",
                        "Minimum Carbon Exports (unit: t/ha)",
                        "Maximum Carbon Exports (unit: t/ha)",
                        "Minimum Tiers 2 Carbon Indicator (unit: tC/ha)",
                        "Maximum Tiers 2 Carbon Indicator (unit: tC/ha)"]

        log.msg("Exporting Carbon Tier2 raster (it might take a while)")
        #print(ras_meta['count'])
        ras_meta['count'] = 6
        ras_meta['dtype'] = "float32"
        #name = raster.split("/")[-1].split(".")[0]
        #output_tiff = "uc1b_carbon_indicator_tier2_%s_%.1f_%.1f.tif"%(name,threshold1,threshold2)
        input_tiff = ct1_raster_file[:-4].split("/")[-1]
        output_tiff = "uc1b_carbon_indicator_tier2_from_%s.tif"%(input_tiff)
        with rasterio.open(output_tiff, 'w', **ras_meta) as dst:
            dst.write(output_raster.astype(np.float32))
            for i,d in enumerate(descriptions):
                dst.set_band_description(i+1, d)


def main_tier1_carbon_indicator(raster,vector,threshold,start,finish,pid,npool,dates_file=None):
    """
    Main function to calculate Tier 1 carbon indicator from input data file and parameters

    Parameters
    ----------
    raster : str
        Name of the NDVI raster image file (TIFF format)
    vector : str
        Name of vector shapefile (ESRI format).
    threshold : float
        NDVI threshold
    start : str
        Starting day of the period on which the indicator need to be caculated ("YYYMMDD" format)
    finish : str
        Finishing day of the period on which the indicator need to be caculated ("YYYMMDD" format)
    pid : str
        Name of the column in the shapefile to be used as an unique identifier. The colunm must contain.
    npool : int
        Number of CPUs to use in parallel for the processing
    dates_file : str(defaul=None)
        Name of the file (ASCII) that contain NDVI acquisition dates. If not provided, dates need to be provided as metadata of the raster image file.
    """

    # Extract raster NDVI data
    log.msg("Opening Rasterize image %s (it might take a while)"%(raster))
    try:
        with rasterio.open(raster) as src:
            ras_meta = src.profile
            log.msg("Raster crs: %s"%(src.crs),"DEBUG")
            destination = Proj(src.crs)
            transform = src.transform
            im = src.read(out_dtype = rasterio.int16) # Note: Band indexing start at 1

            # Rescale if multiplicator format
            if np.max(im) > 1:
                im = im/1000.0

            # Create needed dates structures
            DateErrorMsg = "Dates list metadata are incorrect. Please check or provide a dates list text file with the option -dl (see help)" 
            if dates_file == None:
                dates = []
                for d in src.descriptions:
                    if d == None:
                        log.msg(DateErrorMsg,"ERROR")
                        sys.exit(1)
                    if "NDVI on the" not in d:
                        log.msg(DateErrorMsg,"ERROR")
                        sys.exit(1)

                    dates.append(int(d[12:]))
                dates = np.array(dates)
                log.msg("Dates list obtained from images metadata")
                if len(dates) != im.shape[0]:
                    log.msg(DateErrorMsg,"ERROR")
                    sys.exit(1)

            else:
                dates = np.loadtxt(dates_file).astype(np.int)

    except Exception as e:
        log.msg(e,"ERROR")
        sys.exit(1)


    tidx = dates*0
    for i,d in enumerate(dates):
        tidx[i] = (datetime.datetime.strptime(str(d),'%Y%m%d') - datetime.datetime(1970,1,1)).days

    # Rasterize shapefile - extract pid iformation
    log.msg("Opening vector Shapefile %s (it might take a while)"%(vector))
    feat,table = get_geometries_id(vector,pid)
    shape = im[0].shape
    log.msg("Number of feature: %d"%(len(feat)),"DEBUG")
    parcel = features.rasterize(shapes=feat, out_shape = shape, transform = transform, fill = np.nan)
    parcelIDs = np.unique(parcel)
    parcelIDs = parcelIDs[~np.isnan(parcelIDs)]
    log.msg("Number of shapefile features found: %d"%(len(parcelIDs)))
    #log.msg("Max id: %d"%(np.max(parcelIDs)))

    # Prepare output stucture. output_raster will contain(1) Parcel IDs (2) Nb Days veg (3) CO2 flux
    output_raster = (np.nan*im[:4].copy()).astype(np.float32) # Fast whay of doing so.
    output_raster[0] = parcel

    # Get mask coordinate for reconstruction
    mask = np.greater(parcel,-1) # Normal mask
    #DEBUG
    #mask = np.greater(parcel,30000) # really small subset for debugging
    #mask = np.ones(parcel.shape).astype(np.bool) # All pixels to test performances


    X,Y = np.where(mask)
    # Redefine period
    if(start != None):
        sidx = (datetime.datetime.strptime(start,'%Y%m%d') - datetime.datetime(1970,1,1)).days
        startstr = start
    else:
        sidx = tidx[0]
        startstr = dates[0]

    if(finish != None):
        fidx = (datetime.datetime.strptime(finish,'%Y%m%d') - datetime.datetime(1970,1,1)).days
        finishstr = finish
    else:
        fidx  = tidx[-1]
        finishstr = dates[-1]
    
    log.msg("Time period: [%s,%s]"%(startstr,finishstr))

    ndvi = im[:,mask]
    
    tag = {}
    for i,t in enumerate(tidx):
        tag[t] = dates[i]

    if npool == 1:
        # Special case for npool = 1
        log.msg("Starting 1 sequential block")
        inputdata = (0,X,Y,ndvi,threshold,tidx,sidx,fidx)
        outputdata = [carbon_indicator(inputdata)]
    else:
        log.msg("Starting parallel %d blocks"%(npool))
        pool = mp.Pool(npool)
        nblock = int(len(X)/npool) + 1
        #inputdata =  [[i,X[i*nblock:(i + 1)*nblock],Y[i*nblock:(i + 1)*nblock],im,tidx] for i in range((len(X) + nblock - 1) // nblock )] # split inputdata
        inputdata =  [[i,X[i*nblock:(i + 1)*nblock],Y[i*nblock:(i + 1)*nblock],ndvi[:,i*nblock:(i + 1)*nblock],threshold,tidx,sidx,fidx] for i in range((len(X) + nblock - 1) // nblock )] # split inputdata

        outputdata = pool.map(carbon_indicator,inputdata)
        log.msg("Combining parallel block")

    # Combine after pooling (even if npool = 1)
    for output in outputdata:
        for out in output:
            x = out[0]
            y = out[1]
            nb_days_veg = out[2]
            co2_flux    = out[3]
            nb_cycles   = out[4]
            validity    = out[5]
            output_raster[1,x,y] = validity
            output_raster[2,x,y] = nb_days_veg
            output_raster[3,x,y] = co2_flux


            #nb_days_veg = out[2]
            #co2_flux    = out[3]
            #nb_cycles   = out[4]
            #validity    = out[5]
            #output_raster[1,x,y] = nb_days_veg
            #output_raster[2,x,y] = co2_flux
            #output_raster[3,x,y] = nb_cycles
            #output_raster[4,x,y] = validity


    # Ouput in GeoTIFF format
    #descriptions = ["Parcel ID","Number of days of active vegetation (unit: days)", "CO2 flux (unit: g/m^2)","Number of vegetation cycle","Validity-Length of longest data hole"]
    descriptions = ["Parcel ID","Length of longest data hole","Number of days of active vegetation (unit: days)", "CO2 flux (unit: tC/ha)"]
    log.msg("Exporting output raster (it might take a while)")
    ras_meta['count'] = 4
    ras_meta['dtype'] = "float32"
    #output_name = updatename(raster,"Tier1_Raster_%.1f_%s_%s.csv"%(threshold,startstr,finishstr))
    name = raster.split("/")[-1].split(".")[0]
    output_tiff = "uc1b_carbon_indicator_tier1_%s_%s_%s_%.1f.tif"%(name,startstr,finishstr,threshold)
    with rasterio.open(output_tiff, 'w', **ras_meta) as dst:
        dst.update_tags(INPUTFILE_IMAGE_TIME_SERIES=raster.split("/")[-1],
                        INPUTFILE_VECTOR_PARCELS=vector.split("/")[-1],
                        NDVI_THRESOLD=threshold,
                        TIME_PERIOD="[%s,%s]"%(startstr,finishstr))
        dst.write(output_raster)
        for i,d in enumerate(descriptions):
            dst.set_band_description(i+1, d)

    # Gather information at parcel level
    log.msg("Gather information at parcel level")
    data     = []
    data_s4c = []
 
    X = {}
    Y = {}
    wherep = np.where(np.greater(parcel,-2))
    for x,y in zip(wherep[0],wherep[1]):
        par = parcel[x,y]
        try:
            X[par].append(x)
            Y[par].append(y)
        except:
            X[par] = [x]
            Y[par] = [y]
 

    xmax,ymax = im[0,:,:].shape
    for p in parcelIDs:
        # Assure that parcels are at least 1 pixel and do not touch the image edge (i.e. might be incomplete)
        Qcoor = ((len(X[p]) > 0) and 
                (0 not in X[p]) and
                (0 not in Y[p]) and
                ((xmax-1) not in X[p]) and
                ((ymax-1) not in Y[p]))

        if (p != -1 and Qcoor):
            # Grap pixel of parcel p
            masked_im = im[:,X[p],Y[p]].astype(np.float) 
            # To ignore zero value
            isnanbefore = np.sum(np.isnan(masked_im),axis=1)
            iszerobefore = np.sum(np.equal(masked_im,0),axis = 1)
            masked_im[masked_im == 0] = np.nan      # Zero value (~water) -> Invalid 
            masked_im[masked_im == 0.0] = np.nan      # Zero value (~water) -> Invalid 
            masked_im[masked_im == -10] = np.nan # Cloud masked value  -> Ivalid
            masked_im[masked_im == -10.0] = np.nan # Cloud masked value  -> Ivalid
            npixel = masked_im.shape[1]
            pixelmask = np.less(np.sum(np.isnan(masked_im),axis =1)/npixel,0.9) # To keep date with at least 10% valid pixels 

            # Calculate parcel statistics
            #ndvi_avg = np.nan
            #ndvi_std = np.nan
            #warnings.simplefilter("error")
            ndvi_avg = np.nanmean(masked_im,axis=1)
            ndvi_std = np.nanstd(masked_im,axis=1)
           
            ## DEBUG
            #if p==11310 or p==5798:
            #    print(p)
            #    print(npixel)
            #    print("is nan after",np.sum(np.isnan(masked_im),axis =1))
            #    print("prop",np.sum(np.isnan(masked_im),axis =1)/npixel)
            #    print("Date mask",np.less(np.sum(np.isnan(masked_im),axis =1)/npixel,0.9))
            #    print("NDVI mask",np.greater(ndvi_avg,nth))
            #    print(ndvi_avg.shape)
            #    print("mask[26]",masked_im[26])
            #    print("")

            # avoid negative value (involving NaN) and zero
            #valid = np.greater(ndvi_avg,nth)
            valid = np.logical_and(pixelmask,np.greater(ndvi_avg,nth))
            valid_tidx =  tidx[valid]
            valid_dates = np.array([tag[d] for d in valid_tidx])

            npixels = len(X[p])
            nvalid = len(valid_dates)

            valid_ndvi_avg  = ndvi_avg[valid]
            valid_ndvi_std  = ndvi_std[valid]

            # Gather T1 pixels data in SEN4CAP strange parsing 
            nb_days_veg_avg = np.mean(output_raster[1,X[p],Y[p]])
            co2_flux_avg = np.mean(output_raster[2,X[p],Y[p]])

            # Calculate T1 at parcel level (to compare with IGN program)
            alldays, inter_ndvi = interpolate(valid_tidx,valid_ndvi_avg)

            #Reduice to specific period
            period = np.logical_and(np.greater_equal(alldays,sidx),np.less_equal(alldays,fidx))
            alldays_period    = alldays[period]
            inter_ndvi_period = inter_ndvi[period]
            nb_days_veg_p, cum_days_veg_p, co2_flux_p, nb_cycles = calc_co2_flux(inter_ndvi_period,threshold) 

            # TO DO : Add number of cycle information
 
            # Fill output structure
            data_p     = [p,npixels,nvalid,nb_days_veg_avg,co2_flux_avg,nb_days_veg_p,co2_flux_p]
            data_p_s4c = [p,"NDVI"]

            fmt      =  "%d,%d,%d,%.2f,%.2f,%.2f,%.2f"
            fmt_s4c  =  "%d;%s"
            for a,b,c in zip(valid_dates,valid_ndvi_avg,valid_ndvi_std):
                fmt     = fmt     + ",%d,%.2f,%.2f"
                fmt_s4c = fmt_s4c + ";%s;%.3f;%.3f"
                data_p.append(a)
                data_p.append(b)
                data_p.append(c)
                
                new_a = datetime.datetime.strptime(str(a), '%Y%m%d').strftime('%Y-%m-%d')
                data_p_s4c.append(new_a)
                data_p_s4c.append(b)
                data_p_s4c.append(c)


            # Final append
            data.append(fmt%tuple(data_p)) 
            data_s4c.append(fmt_s4c%tuple(data_p_s4c)) 

    # For DEBUG
    #for x in data:
    #    print(x)

    # Exporte to csv file
    now = datetime.datetime.now().strftime("%Y%m%d %H:%M:%S")
    s4c_format = False
    #output_csv = updatename(raster,"Tier1_RasterToVector")
    #output_csv = output_csv.split(".")[0] + "_%.1f_%s_%s.csv"%(threshold,startstr,finishstr)
    #log.msg("Export in %s file"%(output_csv))

    if(s4c_format):
        log.msg("Exporting average NDVI time serie in csv file (it might take a while)")
        output_csv = "uc1b_average_ndvi_time_serie_%s_%s_%s.csv"%(name,startstr,finishstr)
        header0 = "KOD_PB;suffix;date;mean;stdev\n"
        with open(output_csv, mode='w') as csv_file:
            csv_file.write(header0)
            for x in data_s4c:
                csv_file.write(x)
                csv_file.write("\n")
 
    log.msg("Exporting parcel average in csv file (it might take a while)")
    output_csv = "uc1b_carbon_indicator_tier1_%s_%s_%s_%.1f.csv"%(name,startstr,finishstr,threshold)
    header0 = "### NIVA Tier 1 - Parcel Indicator from Pixels One - Created %s (Cesbio - ASP - IGN - 2020) ###\n"%(now)
    header1 = "#pid,n_pixels,n_valid_dates,nb_days_veg_avg,co2_flux_avg,nb_days_veg_p,co2_flux_p,date_1,ndvi_avg_1,ndvi_std_2,date_2, etc.\n"
    with open(output_csv, mode='w') as csv_file:
        csv_file.write(header0)
        csv_file.write(header1)
        for x in data:
            csv_file.write(x)
            csv_file.write("\n")
    
    # DEBUG
    #plt.imshow(np.equal(parcel,30053))
    #plt.show()

# MAIN #######################################################################################

if __name__ == "__main__":
    
    # print cartouche
    print_cartouche(version,vyear,email) 

    # Main parser
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='indicator',help='Types of indicator (CT1, CT2 or NT1)')
    
    # Initialize subparsers
    ct1_parser = subparsers.add_parser("CT1", help = "Calculate NIVA uc1b carbon tier 1 indicator")
    ct2_parser = subparsers.add_parser("CT2", help = "Calculate NIVA uc1b carbon tier 2 indicator")
    nt1_parser = subparsers.add_parser("NT1", help = "Calculate NIVA uc1b nitrate tier 1 indicator")
  
    # Fill Carbon Tier 1 subparser
    ct1_parser.add_argument("-im", "--image", nargs='+', required=True, help="Input images files")
    ct1_parser.add_argument("-vec", "--vector", nargs="+", required=True, help="Input vector files")
    ct1_parser.add_argument("-dl", "--dateslist", required=True,nargs="+", help="Input dates files list in YYYYMMDD format")
    ct1_parser.add_argument("-th", "--threshold",type=float, required=True, help="NDVI threshold")
    ct1_parser.add_argument("-sd", "--startdate", required=True, type = int, help="Start date (YYYYMMDD)")
    ct1_parser.add_argument("-fd", "--finishdate", required=True, type = int, help="Finish date (YYYYMMDD)")
    ct1_parser.add_argument("-pid", "--parcelid", help="Parcels identification code")
    ct1_parser.add_argument("-np", "--nproc", type = int, help="Number of processors (default 1)")
 
    # Fill Carbon Tier 2 subparser
    ct2_parser.add_argument("-ct1", "--carbontier1", required=True, help="Input image file containing carbon tier 1 indicator")
    ct2_parser.add_argument("-int", "--interventions", required=True, help="Input file contening FMIS intervention data")
    ct2_parser.add_argument("-par", "--parcels", required=True, help="Input file contening FMIS data parcel polygons")
    #ct2_parser.add_argument("-pid", "--parcelid", help="Parcels identification code")
    #ct2_parser.add_argument("-cr", "--crop", required=True,help="Name of the crop column in the vector files")
    #ct2_parser.add_argument("-co", "--conversion", required=True,help="File to convert FMIS class nomenclature")
    #ct2_parser.add_argument("-np", "--nproc", type = int, help="Number of processors (default 1)")
 
    # Fill Nitrate Tier 1 subparser
    nt1_parser.add_argument("-im", "--image", nargs='+', required=True, help="Input image files")
    nt1_parser.add_argument("-vec", "--vector", nargs="+", required=True, help="Input vector file")
    nt1_parser.add_argument("-dl", "--dateslist", required=True,nargs="+", help="Input dates files list in YYYYMMDD format")
    nt1_parser.add_argument("-sd", "--startdate", required=True, type = int, help="Start date (YYYYMMDD)")
    nt1_parser.add_argument("-fd", "--finishdate", required=True, type = int, help="Finish date (YYYYMMDD)")
    nt1_parser.add_argument("-cr", "--crop", required=True,help="Name of the crop field in the vector files")
    nt1_parser.add_argument("-co", "--conversion", required=True,help="File to convert crop label in the crop field to extended LUCAS nomenclature")
    nt1_parser.add_argument("-np", "--nproc", type = int, help="Number of processors (default 1)")
 
    args = parser.parse_args()
    if args.indicator == None:
        parser.print_help()
        parser.error("%s: error: too few arguments"%(__file__))
        #print("%s: error: too few arguments"%(__file__))
        sys.exit(1)
 
    elif args.indicator != "CT2":
        # Get date variables
        start = str(args.startdate)
        finish = str(args.finishdate)

        # Get number of processor or fix it to 1 by default
        if args.nproc==None:
            npool = 1
        else:
            npool = args.nproc
 

    if args.indicator == "CT1":
        for i,raster_file in enumerate(args.image):
            pid = args.parcelid
            if args.dateslist != None: # Can work if date list is provided as metadata. however deasctivated for simplicity
                if (len(args.image) == len(args.dateslist) and
                    len(args.image) == len(args.vector) and 
                    len(args.vector) == len(args.dateslist)):
 
                    log.msg("*** Calculating Carbon Indicator on file %s ***"%(raster_file))
                    dates_file = args.dateslist[i]
                    vector_file = args.vector[i]
                    threshold = args.threshold
                    main_tier1_carbon_indicator(raster_file,vector_file,threshold,start,finish,pid,npool,dates_file = dates_file)
                else:
                    parser.error("Each parameters -im, -vec and -dl mut contains the same number of elements")
 
 
    if args.indicator == "NT1":
        for i,raster_file in enumerate(args.image):
            if args.dateslist != None:
                if len(args.image) != len(args.dateslist):
                    parser.error("The lists -im and -dl must have the same size")
 
            if 2*len(args.image) != len(args.vector):
                parser.error("The lists -vec must have twice the size of the list -im")
 
            #if (args.crop==None or
            #   args.conversion==None):
            #   parser.error("Nitrate Indicator (-ind N) requires --cr and --co")
            #   sys.exit(1)
 
            log.msg("*** Calculating Nitrate Indicator on file %s ***"%(raster_file))
            dates_file = args.dateslist[i]
            vector_file1 = args.vector[2*i] 
            vector_file2 = args.vector[2*i+1]
            crop_column = args.crop
            conversion_file = args.conversion
            main_tier1_nitrate_indicator(raster_file,vector_file1,vector_file2,th1,th2,crop_column,conversion_file,npool,start,finish,dates_file = dates_file)
 
    if args.indicator == "CT2":
        npool = 1
        ct1_image     = args.carbontier1
        interventions = args.interventions
        parcels       = args.parcels
        #log.msg('''*** Carbon Indicator Tier2 is not implemented yet, It is coming really soon ! ***''')
        log.msg('''*** Calculating Carbon Indicator Tier2 from Carbon Indicator Tier1 contained in file ***
                            *** %s ***'''%(ct1_image))
        main_tier2_carbon_indicator(ct1_image,interventions,parcels,npool)

    # END
    log.msg("Done")   
