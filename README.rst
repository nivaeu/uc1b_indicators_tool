Introduction
------------
This sub-project is part of the NIVA_ project that delivers a suite of digital solutions, e-tools and good practices for e-governance and initiates an innovation ecosystem to support further development of IACS that will facilitate data and information flows.

This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 842009.

Please visit the NIVA_ website for further information. A complete list of the sub-projects made available under the NIVA project can be found in github_


NIVA UC1b carbon flux and nitrate leaching indicators at pixel level
--------------------------------------------------------------------
- This tool implements the calculation of the carbon flux indicator (Tier 1 Carbon), the carbon budget indicator (tier 2 Carbon) and the nitrate leaching indicator (Tier 1 Nitrate) at pixel level.
- Installation details can be found in INSTALL.rst
- Complete documentation in html format can be found in docs/build/html/index.html
- Quality Assessment of the Carbon Tier 1 indicator can be found at doc/NIVA_UC1b_Carbon_Tier_I_Quality_Assessment-v0.1.docx 
- Atlas on France of the Carbon Tier 1 indicator can be found at docs/Atlas_NIVA_ProjetENSG.pdf

NIVA UC1b CT3 annual carbon budget
----------------------------------
For technical and licence reasons, the NIVA UC1b CT3 annual carbon budget indicator is produced thanks to another tool, specific to the problem methodology. This tool calculates the annual carbon budget indicator thanks to the assimilation of Sentinel-2 data in a physical agronomical model called SAFYE-CO2. This tool can be used on a relatively small surface, typically over about 100 parcels. It is available as open source here (https://framagit.org/ahmad.albitar/safye_co2.git). The same methodology can be applied on a bigger surface thanks to the Agricarbon-EO processing chain. However, this chain is not available as an open source tool. More about AgriCarbon-EO can be found here (https://www.cesbio.cnrs.fr/agricarboneo/agricarbon-eo/) concerning scientific details and here (https://www.toulouse-tech-transfer.com/) concernig pintellectual property and private exploitation.



|EU-PL 1.2 shield|

The source code relative to this work is licensed under the EUPL-1.2-or-later.

|CC BY-SA 4.0 shield|

The dataset relative to this work is licensed under a `Creative Commons Attribution 4.0
International License <http://creativecommons.org/licenses/by-sa/4.0/>`__.

|CC BY-SA 4.0|


.. _NIVA: https://www.niva4cap.eu/
.. _github: https://gitlab.com/nivaeu/
.. _safyco2: https://framagit.org/ahmad.albitar/safye_co2.git
.. _aceo: https://www.cesbio.cnrs.fr/agricarboneo/agricarbon-eo/
.. _ttt: https://www.toulouse-tech-transfer.com/
.. |EU-PL 1.2 shield| image:: https://img.shields.io/badge/license-EUPL-green.svg
   :target: https://opensource.org/licenses/EUPL-1.2
.. |CC BY-SA 4.0 shield| image:: https://img.shields.io/badge/License-CC%20BY--SA%204.0-green.svg
   :target: http://creativecommons.org/licenses/by-sa/4.0/
.. |CC BY-SA 4.0| image:: https://licensebuttons.net/l/by-sa/4.0/88x31.png
   :target: http://creativecommons.org/licenses/by-sa/4.0/
